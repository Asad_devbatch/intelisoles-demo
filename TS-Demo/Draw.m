//
//  Draw.m
//  TB3
//
//  Created by Earl Van Wagoner on 1/19/13.
//  Copyright (c) 2013 ST alliance AS. All rights reserved.
//

#import "Draw.h"
#import "TSViewController.h"

@implementation Draw


//NSInteger height = 100;
//NSInteger width = 200;
NSInteger verticalpos;
NSInteger horizontalpos;

NSInteger ChangeColor1 = 1;
NSInteger ChangeColor2 = 0;
NSInteger ChangeColor3 = 0;
NSInteger ChangeColor4 = 0;
NSInteger StartDraw = 0;
NSInteger StopDraw = 0;
NSInteger ClearDraw = 0;
NSInteger AlarmLine = 0;

int oldX = 0;
int oldY = 0;

bool FirstTime = true;

CGContextRef drawingContext;


- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder: aDecoder])
    {
        self.backgroundColor = [UIColor clearColor];
//        self.backgroundColor = [UIColor whiteColor];
        CGSize size = self.bounds.size;
        drawingContext = [self createOffScreenContext: size];
    }
    
    return self;
}

// This creates the context we can push and pop without clearing.
- (CGContextRef) createOffScreenContext: (CGSize) size
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(NULL, size.width, size.height, 8, size.width*4, colorSpace,kCGImageAlphaPremultipliedLast);
    
    CGColorSpaceRelease(colorSpace);
    
    CGContextTranslateCTM(context, 0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
/*    CGContextSetLineWidth(context, 1);
    CGContextSetStrokeColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextMoveToPoint(context, 0, 125);
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextAddLineToPoint(context, 500, 125);
    CGContextStrokePath(context);
    CGContextMoveToPoint(context, 250, 0);
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextAddLineToPoint(context, 250, 250);
    CGContextStrokePath(context);
*/
    return context;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = [UIColor clearColor];
        CGSize size = self.bounds.size;
        drawingContext = [self createOffScreenContext: size];
        
    }
    return self;
}

- (void)updateData
{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (StartDraw == 0) {
        CGContextSetLineWidth(context, 2);
        CGContextSetStrokeColorWithColor(context, [[UIColor clearColor] CGColor]);
        CGContextMoveToPoint(context, oldX, oldY);
        CGContextAddLineToPoint(context, horizontalpos, verticalpos);
        oldX = horizontalpos;
        oldY = verticalpos;
        CGContextStrokePath(context);
        //FirstTime = false;
    }
    else {
        CGContextSetLineWidth(context, 2);
        if (ChangeColor1 == 1) CGContextSetStrokeColorWithColor(context, [[UIColor redColor] CGColor]);
        else if (ChangeColor2 == 1) CGContextSetStrokeColorWithColor(context, [[UIColor orangeColor] CGColor]);
        else if (ChangeColor3 == 1) CGContextSetStrokeColorWithColor(context, [[UIColor yellowColor] CGColor]);
        else CGContextSetStrokeColorWithColor(context, [[UIColor greenColor] CGColor]);
        
        if (AlarmLine == 1)
            CGContextSetStrokeColorWithColor(context, [[UIColor whiteColor] CGColor]);
        
//        CGContextSetStrokeColorWithColor(context, [[UIColor blueColor] CGColor]);
        CGContextMoveToPoint(context, oldX, oldY);
        CGContextAddLineToPoint(context, horizontalpos, verticalpos);
        oldX = horizontalpos;
        oldY = verticalpos;
        CGContextStrokePath(context);
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.


- (void)drawRect:(CGRect)rect
{
    // push the context on which represent an offscreen bitmap
    UIGraphicsPushContext(drawingContext);
    
    if (ClearDraw == 1) {
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextClearRect(context, rect);
        
/*        CGContextSetLineWidth(context, 1);
        CGContextSetStrokeColorWithColor(context, [[UIColor clearColor] CGColor]);
        CGContextMoveToPoint(context, 0, 125);
        CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
        CGContextAddLineToPoint(context, 500, 125);
        CGContextStrokePath(context);
        CGContextMoveToPoint(context, 250, 0);
        CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
        CGContextAddLineToPoint(context, 250, 250);
        CGContextStrokePath(context);
*/
        ClearDraw = 0;
    }
    
//    CGContextSetLineWidth(drawingContext, 0.6);
//    CGContextSetStrokeColorWithColor(drawingContext, [[UIColor lightGrayColor] CGColor]);
    
/*
    
    //TODO: optimize this - since we're using an image to draw onto, only draw the grid once
    CGFloat dash[] = {6.0, 3.0};
    CGContextSetLineDash(drawingContext, 0.0, dash, 2);
    
    // how many vertical lines...
    int howManyVertical = (kDefaultGraphWidth - kOffsetX) / kStepX;
    for (int i=0; i < howManyVertical; i++)
    {
        CGContextMoveToPoint(drawingContext, kOffsetX + i * kStepX, kGraphTop);
        CGContextAddLineToPoint(drawingContext, kOffsetX + i * kStepX, kGraphBottom);
    }
    
    // how many horizontal lines...
    int howManyHorizontal = (kGraphBottom - kGraphTop - kOffsetY) / kStepY;
    for (int i=0; i <= howManyHorizontal; i++)
    {
        CGContextMoveToPoint(drawingContext, kOffsetX, kGraphBottom - kOffsetY - i * kStepY);
        CGContextAddLineToPoint(drawingContext, kDefaultGraphWidth, kGraphBottom - kOffsetY - i * kStepY);
    }
    
    CGContextStrokePath(drawingContext);
    CGContextSetLineDash(drawingContext, 0, NULL, 0); // Remove the dash...

*/
 
    [self updateData];
    
    // grab the image from the context so we can blit it using drawInRect with the
    //  rect that was passed in here
    CGImageRef cgImage = CGBitmapContextCreateImage(drawingContext);
    UIImage *uiImage = [[UIImage alloc] initWithCGImage:cgImage];
    UIGraphicsPopContext();
    CGImageRelease(cgImage);
    
    // draw the image we've been drawing to
    [uiImage drawInRect: rect];
    
    uiImage = 0;
    FirstTime = false;
}



@end
