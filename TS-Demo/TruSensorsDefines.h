//
//  TruSensorsDefines.h
//  TruSensors
//

#ifndef TruSensorsDefines_h
#define TruSensorsDefines_h

// Defines for the TruSensors peripheral

#define TS_PROXIMITY_ALERT_UUID                      0x1802
#define TS_PROXIMITY_ALERT_PROPERTY_UUID             0x2a06
#define TS_PROXIMITY_ALERT_ON_VAL                    0x01
#define TS_PROXIMITY_ALERT_OFF_VAL                   0x00
#define TS_PROXIMITY_ALERT_WRITE_LEN                 1
#define TS_PROXIMITY_TX_PWR_SERVICE_UUID             0x1804
#define TS_PROXIMITY_TX_PWR_NOTIFICATION_UUID        0x2A07
#define TS_PROXIMITY_TX_PWR_NOTIFICATION_READ_LEN    1

#define TS_BATT_SERVICE_UUID                         0x180F
#define TS_LEVEL_SERVICE_UUID                        0x2A19
#define TS_POWER_STATE_UUID                          0xFFB2
#define TS_LEVEL_SERVICE_READ_LEN                    1

#define TS_FSR_SERVICE_UUID                        0xFFA0
#define TS_FSR_ENABLER_UUID                        0xFFA1
#define TS_FSR_RANGE_UUID                          0xFFA2
#define TS_FSR_READ_LEN                            1
#define TS_FSR_1_UUID                              0xFFA3
#define TS_FSR_2_UUID                              0xFFA4
#define TS_FSR_3_UUID                              0xFFA5
#define TS_FSR_4_UUID                              0xFFA6

#define TS_KEYS_SERVICE_UUID                         0xFFE0
#define TS_KEYS_NOTIFICATION_UUID                    0xFFE1
#define TS_KEYS_NOTIFICATION_READ_LEN                1

#endif
