//
//  Draw.h
//  TB3
//
//  Created by Earl Van Wagoner on 1/19/13.
//  Copyright (c) 2013 ST alliance AS. All rights reserved.
//

#import <UIKit/UIKit.h>



extern NSInteger horizontalpos;
extern NSInteger verticalpos;

extern NSInteger ChangeColor1;
extern NSInteger ChangeColor2;
extern NSInteger ChangeColor3;
extern NSInteger ChangeColor4;
extern NSInteger StartDraw;
extern NSInteger StopDraw;
extern NSInteger ClearDraw;
extern NSInteger AlarmLine;

int oldX;
int oldY;
bool FirstTime;


@interface Draw : UIView

- (void)updateData;


@end
