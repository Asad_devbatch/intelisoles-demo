//
// UIColorSelection.h
//
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorSelection)

/// @return A random color with alpha of 1.0.
+ (UIColor *)randomColor;

/// @return A random color with random alpha.
+ (UIColor *)randomColorAndAlpha;

// Colors found in the Mac crayon box color picker.

+ (UIColor *)aluminumCrayonColor;
+ (UIColor *)aquaCrayonColor;
+ (UIColor *)asparagusCrayonColor;
+ (UIColor *)bananaCrayonColor;
+ (UIColor *)blueberryCrayonColor;
+ (UIColor *)bubblegumCrayonColor;
+ (UIColor *)carnationCrayonColor;
+ (UIColor *)cantalopeCrayonColor;
+ (UIColor *)cayenneCrayonColor;
+ (UIColor *)cloverCrayonColor;
+ (UIColor *)eggplantCrayonColor;
+ (UIColor *)fernCrayonColor;
+ (UIColor *)floraCrayonColor;
+ (UIColor *)grapeCrayonColor;
+ (UIColor *)honeydewCrayonColor;
+ (UIColor *)iceCrayonColor;
+ (UIColor *)ironCrayonColor;
+ (UIColor *)lavenderCrayonColor;
+ (UIColor *)leadCrayonColor;
+ (UIColor *)lemonCrayonColor;
+ (UIColor *)licoriceCrayonColor;
+ (UIColor *)limeCrayonColor;
+ (UIColor *)magentaCrayonColor;
+ (UIColor *)magnesiumCrayonColor;
+ (UIColor *)maraschinoCrayonColor;
+ (UIColor *)maroonCrayonColor;
+ (UIColor *)mercuryCrayonColor;
+ (UIColor *)midnightCrayonColor;
+ (UIColor *)mochaCrayonColor;
+ (UIColor *)mossCrayonColor;
+ (UIColor *)nickelCrayonColor;
+ (UIColor *)oceanCrayonColor;
+ (UIColor *)orchidCrayonColor;
+ (UIColor *)plumCrayonColor;
+ (UIColor *)salmonCrayonColor;
+ (UIColor *)seafoamCrayonColor;
+ (UIColor *)silverCrayonColor;
+ (UIColor *)skyCrayonColor;
+ (UIColor *)snowCrayonColor;
+ (UIColor *)spindriftCrayonColor;
+ (UIColor *)springCrayonColor;
+ (UIColor *)steelCrayonColor;
+ (UIColor *)strawberryCrayonColor;
+ (UIColor *)tangerineCrayonColor;
+ (UIColor *)tealCrayonColor;
+ (UIColor *)tinCrayonColor;
+ (UIColor *)tungstenCrayonColor;
+ (UIColor *)turquoiseCrayonColor;

// Standard HTML colors

+ (UIColor *)aliceBlueHTMLColor;
+ (UIColor *)antiqueWhiteHTMLColor;
+ (UIColor *)aquaHTMLColor;
+ (UIColor *)aquamarineHTMLColor;
+ (UIColor *)azureHTMLColor;
+ (UIColor *)beigeHTMLColor;
+ (UIColor *)bisqueHTMLColor;
+ (UIColor *)blackHTMLColor;
+ (UIColor *)blanchedAlmondHTMLColor;
+ (UIColor *)blueHTMLColor;
+ (UIColor *)blueVioletHTMLColor;
+ (UIColor *)brownHTMLColor;
+ (UIColor *)burlyWoodHTMLColor;
+ (UIColor *)cadetBlueHTMLColor;
+ (UIColor *)chartreuseHTMLColor;
+ (UIColor *)chocolateHTMLColor;
+ (UIColor *)coralHTMLColor;
+ (UIColor *)cornflowerBlueHTMLColor;
+ (UIColor *)cornsilkHTMLColor;
+ (UIColor *)crimsonHTMLColor;
+ (UIColor *)cyanHTMLColor;
+ (UIColor *)darkBlueHTMLColor;
+ (UIColor *)darkCyanHTMLColor;
+ (UIColor *)darkGoldenRodHTMLColor;
+ (UIColor *)darkGrayHTMLColor;
+ (UIColor *)darkGreyHTMLColor;
+ (UIColor *)darkGreenHTMLColor;
+ (UIColor *)darkKhakiHTMLColor;
+ (UIColor *)darkMagentaHTMLColor;
+ (UIColor *)darkOliveGreenHTMLColor;
+ (UIColor *)darkorangeHTMLColor;
+ (UIColor *)darkOrchidHTMLColor;
+ (UIColor *)darkRedHTMLColor;
+ (UIColor *)darkSalmonHTMLColor;
+ (UIColor *)darkSeaGreenHTMLColor;
+ (UIColor *)darkSlateBlueHTMLColor;
+ (UIColor *)darkSlateGrayHTMLColor;
+ (UIColor *)darkSlateGreyHTMLColor;
+ (UIColor *)darkTurquoiseHTMLColor;
+ (UIColor *)darkVioletHTMLColor;
+ (UIColor *)deepPinkHTMLColor;
+ (UIColor *)deepSkyBlueHTMLColor;
+ (UIColor *)dimGrayHTMLColor;
+ (UIColor *)dimGreyHTMLColor;
+ (UIColor *)dodgerBlueHTMLColor;
+ (UIColor *)fireBrickHTMLColor;
+ (UIColor *)floralWhiteHTMLColor;
+ (UIColor *)forestGreenHTMLColor;
+ (UIColor *)fuchsiaHTMLColor;
+ (UIColor *)gainsboroHTMLColor;
+ (UIColor *)ghostWhiteHTMLColor;
+ (UIColor *)goldHTMLColor;
+ (UIColor *)goldenRodHTMLColor;
+ (UIColor *)grayHTMLColor;
+ (UIColor *)greyHTMLColor;
+ (UIColor *)greenHTMLColor;
+ (UIColor *)greenYellowHTMLColor;
+ (UIColor *)honeyDewHTMLColor;
+ (UIColor *)hotPinkHTMLColor;
+ (UIColor *)indianRedHTMLColor;
+ (UIColor *)indigoHTMLColor;
+ (UIColor *)ivoryHTMLColor;
+ (UIColor *)khakiHTMLColor;
+ (UIColor *)lavenderHTMLColor;
+ (UIColor *)lavenderBlushHTMLColor;
+ (UIColor *)lawnGreenHTMLColor;
+ (UIColor *)lemonChiffonHTMLColor;
+ (UIColor *)lightBlueHTMLColor;
+ (UIColor *)lightCoralHTMLColor;
+ (UIColor *)lightCyanHTMLColor;
+ (UIColor *)lightGoldenRodYellowHTMLColor;
+ (UIColor *)lightGrayHTMLColor;
+ (UIColor *)lightGreyHTMLColor;
+ (UIColor *)lightGreenHTMLColor;
+ (UIColor *)lightPinkHTMLColor;
+ (UIColor *)lightSalmonHTMLColor;
+ (UIColor *)lightSeaGreenHTMLColor;
+ (UIColor *)lightSkyBlueHTMLColor;
+ (UIColor *)lightSlateGrayHTMLColor;
+ (UIColor *)lightSlateGreyHTMLColor;
+ (UIColor *)lightSteelBlueHTMLColor;
+ (UIColor *)lightYellowHTMLColor;
+ (UIColor *)limeHTMLColor;
+ (UIColor *)limeGreenHTMLColor;
+ (UIColor *)linenHTMLColor;
+ (UIColor *)magentaHTMLColor;
+ (UIColor *)maroonHTMLColor;
+ (UIColor *)mediumAquaMarineHTMLColor;
+ (UIColor *)mediumBlueHTMLColor;
+ (UIColor *)mediumOrchidHTMLColor;
+ (UIColor *)mediumPurpleHTMLColor;
+ (UIColor *)mediumSeaGreenHTMLColor;
+ (UIColor *)mediumSlateBlueHTMLColor;
+ (UIColor *)mediumSpringGreenHTMLColor;
+ (UIColor *)mediumTurquoiseHTMLColor;
+ (UIColor *)mediumVioletRedHTMLColor;
+ (UIColor *)midnightBlueHTMLColor;
+ (UIColor *)mintCreamHTMLColor;
+ (UIColor *)mistyRoseHTMLColor;
+ (UIColor *)moccasinHTMLColor;
+ (UIColor *)navajoWhiteHTMLColor;
+ (UIColor *)navyHTMLColor;
+ (UIColor *)oldLaceHTMLColor;
+ (UIColor *)oliveHTMLColor;
+ (UIColor *)oliveDrabHTMLColor;
+ (UIColor *)orangeHTMLColor;
+ (UIColor *)orangeRedHTMLColor;
+ (UIColor *)orchidHTMLColor;
+ (UIColor *)paleGoldenRodHTMLColor;
+ (UIColor *)paleGreenHTMLColor;
+ (UIColor *)paleTurquoiseHTMLColor;
+ (UIColor *)paleVioletRedHTMLColor;
+ (UIColor *)papayaWhipHTMLColor;
+ (UIColor *)peachPuffHTMLColor;
+ (UIColor *)peruHTMLColor;
+ (UIColor *)pinkHTMLColor;
+ (UIColor *)plumHTMLColor;
+ (UIColor *)powderBlueHTMLColor;
+ (UIColor *)purpleHTMLColor;
+ (UIColor *)redHTMLColor;
+ (UIColor *)rosyBrownHTMLColor;
+ (UIColor *)royalBlueHTMLColor;
+ (UIColor *)saddleBrownHTMLColor;
+ (UIColor *)salmonHTMLColor;
+ (UIColor *)sandyBrownHTMLColor;
+ (UIColor *)seaGreenHTMLColor;
+ (UIColor *)seaShellHTMLColor;
+ (UIColor *)siennaHTMLColor;
+ (UIColor *)silverHTMLColor;
+ (UIColor *)skyBlueHTMLColor;
+ (UIColor *)slateBlueHTMLColor;
+ (UIColor *)slateGrayHTMLColor;
+ (UIColor *)slateGreyHTMLColor;
+ (UIColor *)snowHTMLColor;
+ (UIColor *)springGreenHTMLColor;
+ (UIColor *)steelBlueHTMLColor;
+ (UIColor *)tanHTMLColor;
+ (UIColor *)tealHTMLColor;
+ (UIColor *)thistleHTMLColor;
+ (UIColor *)tomatoHTMLColor;
+ (UIColor *)turquoiseHTMLColor;
+ (UIColor *)violetHTMLColor;
+ (UIColor *)wheatHTMLColor;
+ (UIColor *)whiteHTMLColor;
+ (UIColor *)whiteSmokeHTMLColor;
+ (UIColor *)yellowHTMLColor;
+ (UIColor *)yellowGreenHTMLColor;

@end