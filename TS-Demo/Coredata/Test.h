//
//  Test.h
//  InteliSoles
//
//  Created by Asad Ali on 16/01/2015.
//  Copyright (c) 2015 ST alliance AS. All rights reserved.
//

#import "CHStorageHelper.h"

@class Patient;

@interface Test : CHStorageHelper

@property (nonatomic, retain) NSDate * testCreatedAt;
@property (nonatomic, retain) id testData;
@property (nonatomic, retain) NSNumber * testId;
@property (nonatomic, retain) Patient *testPatient;

@end
