//
//  Patient.m
//  InteliSoles
//
//  Created by Asad Ali on 16/01/2015.
//  Copyright (c) 2015 ST alliance AS. All rights reserved.
//

#import "Patient.h"


@implementation Patient

@dynamic patientDoB;
@dynamic patientId;
@dynamic patientIsActive;
@dynamic patientName;
@dynamic patientTests;

@end
