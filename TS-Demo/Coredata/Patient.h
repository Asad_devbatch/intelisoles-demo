//
//  Patient.h
//  InteliSoles
//
//  Created by Asad Ali on 16/01/2015.
//  Copyright (c) 2015 ST alliance AS. All rights reserved.
//

#import "CHStorageHelper.h"


@interface Patient : CHStorageHelper

@property (nonatomic, retain) NSDate * patientDoB;
@property (nonatomic, retain) NSString * patientId;
@property (nonatomic, retain) NSNumber * patientIsActive;
@property (nonatomic, retain) NSString * patientName;
@property (nonatomic, retain) NSSet *patientTests;
@end

@interface Patient (CoreDataGeneratedAccessors)

- (void)addPatientTestsObject:(NSManagedObject *)value;
- (void)removePatientTestsObject:(NSManagedObject *)value;
- (void)addPatientTests:(NSSet *)values;
- (void)removePatientTests:(NSSet *)values;

@end
