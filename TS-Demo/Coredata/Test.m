//
//  Test.m
//  InteliSoles
//
//  Created by Asad Ali on 16/01/2015.
//  Copyright (c) 2015 ST alliance AS. All rights reserved.
//

#import "Test.h"
#import "Patient.h"


@implementation Test

@dynamic testCreatedAt;
@dynamic testData;
@dynamic testId;
@dynamic testPatient;

@end
