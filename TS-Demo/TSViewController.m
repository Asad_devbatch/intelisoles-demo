//
//  TSViewController.m
//  TruSensors
//

#import "TSViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "Draw.h"
#import <ShinobiCharts/ShinobiChart.h>
#import "UIColorSelection.h"

#define kChartLicenseKey @"N18HT0kebImWh5FMjAxNTAyMTRhc2FkQGRldmJhdGNoLmNvbQ==qo7gM0MHyRnlXX9Mw67O87UP8WKg9ThnlYqQR+k+Sa1MIZz5MlKAZavw6HyMbY4Qt4S/+dcMULKdeDeJPfSWOYlU18tiPPokbTR0j0RA7blK+XfmCImh9vWQe0KGoa52Ecz8Huq2CW2iabmtqkgb0suEMbow=BQxSUisl3BaWf/7myRmmlIjRnMU2cA7q+/03ZX9wdj30RzapYANf51ee3Pi8m2rVW6aD7t6Hi4Qy5vv9xpaQYXF5T7XzsafhzS3hbBokp36BoJZg8IrceBj742nQajYyV7trx5GIw9jy/V6r0bvctKYwTim7Kzq+YPWGMtqtQoU=PFJTQUtleVZhbHVlPjxNb2R1bHVzPnh6YlRrc2dYWWJvQUh5VGR6dkNzQXUrUVAxQnM5b2VrZUxxZVdacnRFbUx3OHZlWStBK3pteXg4NGpJbFkzT2hGdlNYbHZDSjlKVGZQTTF4S2ZweWZBVXBGeXgxRnVBMThOcDNETUxXR1JJbTJ6WXA3a1YyMEdYZGU3RnJyTHZjdGhIbW1BZ21PTTdwMFBsNWlSKzNVMDg5M1N4b2hCZlJ5RHdEeE9vdDNlMD08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+"

OSStatus RenderTone(
                    void *inRefCon,
                    AudioUnitRenderActionFlags 	*ioActionFlags,
                    const AudioTimeStamp 		*inTimeStamp,
                    UInt32 						inBusNumber,
                    UInt32 						inNumberFrames,
                    AudioBufferList 			*ioData)

{
	// Fixed amplitude is good enough for our purposes
    //	const double amplitude = 0.25;
    
	// Get the tone parameters out of the view controller
	TSViewController *viewController =
    (__bridge TSViewController *)inRefCon;
    double amplitude = viewController->volume;
	double theta = viewController->theta;
	double theta_increment = 2.0 * M_PI * viewController->frequency / viewController->sampleRate;
    
	// This is a mono tone generator so we only need the first buffer
	const int channel = 0;
	Float32 *buffer = (Float32 *)ioData->mBuffers[channel].mData;
	
	// Generate the samples
	for (UInt32 frame = 0; frame < inNumberFrames; frame++)
	{
		buffer[frame] = sin(theta) * amplitude;
		
		theta += theta_increment;
		if (theta > 2.0 * M_PI)
		{
			theta -= 2.0 * M_PI;
		}
	}
	
	// Store the theta back in the view controller
	viewController->theta = theta;
    
	return noErr;
}

void ToneInterruptionListener(void *inClientData, UInt32 inInterruptionState)
{
	TSViewController *viewController =
    (__bridge TSViewController *)inClientData;
	
	[viewController stop];
}

UInt8 Final1X;
UInt8 Final1Y;
UInt8 Final1Z;
UInt8 Final14;
UInt8 Final2X;
UInt8 Final2Y;
UInt8 Final2Z;
UInt8 Final24;
UInt8 Final3X;
UInt8 Final3Y;
UInt8 Final3Z;


@interface TSViewController () <SChartDatasource>

@property (nonatomic, strong) NSMutableArray *testValues;

@end

@implementation TSViewController
{
    ShinobiChart *_chart1;
    NSArray *rowArray;
    NSMutableArray *firstDevArray;
    NSMutableArray *secondDevArray;
    NSMutableArray *thirdDevArray;
    NSMutableArray *fourthDevArray;
    NSMutableArray *fifthDevArray;
    NSMutableArray *sixthDevArray;
    NSMutableArray *seventhDevArray;
    NSMutableArray *eighthDevArray;
    NSMutableArray *ninthDevArray;
    NSMutableArray *tenthDevArray;
    NSMutableArray *eleventhDevArray;
    
    NSMutableArray *firstFSRArray;
    NSMutableArray *secondFSRArray;
    NSMutableArray *thirdFSRArray;
    NSMutableArray *fourthFSRArray;
    NSMutableArray *fifthFSRArray;
    NSMutableArray *sixthFSRArray;
    NSMutableArray *seventhFSRArray;
    NSMutableArray *eighthFSRArray;
    NSMutableArray *ninthFSRArray;
    NSMutableArray *tenthFSRArray;
    NSMutableArray *eleventhFSRArray;
    
    NSArray *filenames;
    NSArray *tableData;
    
    NSInteger openTestIndex;
}

//@synthesize view;
@synthesize TSUIBatteryBar;
//@synthesize TSUIBatteryBarLabel;
@synthesize TSUIBatteryBarL;
@synthesize TSUIBatteryBarAcc;
//@synthesize TSUIBatteryBarLabelL;
@synthesize TSUILeftButton;
@synthesize TSUIRightButton;
@synthesize TSUISpinner;
@synthesize TSUISpinnerL;
@synthesize TSUISpinnerAcc;
@synthesize verticals, horizontals, lverticals, lhorizontals, RLs;
@synthesize verticals2, horizontals2, lverticals2, lhorizontals2, RLs2;
@synthesize RLindicator, Lhind, Rhind, Lvind, Rvind;
@synthesize acchorizontals, acc45degs, accverticals;

@synthesize target;
@synthesize Ltarget;
@synthesize Rtarget;
@synthesize grid;
@synthesize alarm;
@synthesize accelerometer;
@synthesize background;
@synthesize accelerometerBatt;
@synthesize puttT;
@synthesize puttS;
@synthesize arrow2;
@synthesize arrow4;
@synthesize golfShoes;
@synthesize golfShoeL;
@synthesize golfShoeR;
@synthesize leftFoot;
@synthesize rightFoot;
@synthesize basePlate;

//@synthesize CIColor;
@synthesize resultsView;
@synthesize setupView;
@synthesize balanceView;
@synthesize plotView;
@synthesize fileView;
@synthesize file2View;
@synthesize footView;
@synthesize fileSaveView;

@synthesize frequencySlider;
@synthesize volumeSlider;
@synthesize patHeightSlider;
@synthesize patStanceSlider;
@synthesize playButton;
@synthesize frequencyLabel;
@synthesize volumeLabel;
@synthesize patHeightLabel;
@synthesize patStanceLabel;
@synthesize feetLabel;
@synthesize inchesLabel;
//@synthesize dev1PlotSwitch;
//@synthesize dev2PlotSwitch;
//@synthesize dev3PlotSwitch;

//@synthesize FSsaveButton;

@synthesize textFieldText;

//@synthesize tableViewCell;

float hSliderVal = 50;
float vSliderVal = 50;
float lhSliderVal = 50;
float lvSliderVal = 50;
float hSliderVal2 = 50;
float vSliderVal2 = 50;
float lhSliderVal2 = 50;
float lvSliderVal2 = 50;
float acchSliderVal = 50;
float accvSliderVal = 50;
float acc45degSliderVal = 50;
float RLsVal = 50;
float RLsVal2 = 50;
int RLSliderVal = 50;
float Lsum;
float Rsum;
float LC1 = 0;
float LC2 = 0;
float LC3 = 0;
float LC4 = 0;
float LC1_2, LC3_4, LC1_3, LC2_4;
float tverticalpos = 0;
float thorizontalpos = 0;
float height = 200;
float width = 300;
//float oldX;
//float oldY;
int oldx = 0, oldy = 0, oldz = 0, old4 = 0;

float los_1 = 0, los_2 = 0, los_3 = 0, los_4 = 0, los_5 = 0, los_6 = 0, los_7 = 0, los_8 = 0;
float los_1p = 0, los_2p = 0, los_3p = 0, los_4p = 0, los_5p = 0, los_6p = 0, los_7p = 0, los_8p = 0;

int iOS_offset = 0;

char fsr1ZeroVal1 = 0, fsr2ZeroVal1 = 0, fsr3ZeroVal1 = 0, fsr4ZeroVal1 = 0;
char fsr1ZeroVal2 = 0, fsr2ZeroVal2 = 0, fsr3ZeroVal2 = 0, fsr4ZeroVal2 = 0;

//int dispMode = 0;

bool DisplayIndicator = true;
bool LimitsIndicator = false;
bool TargetSelected = true;
bool AlarmSelected = false;
bool GridSelected = false;
bool ShoesSelected = false;
bool RawSelected = false;
bool AccSelected = false;
bool PuttSelected = true;
bool ZeroFSRSelected = false;
bool ZeroFSR1 = false;
bool ZeroFSR2 = false;
bool More1Selected = false;
bool More2Selected = false;
bool PlotSelected = false;

bool GraphOnly = false;

bool BalanceMode = false;

bool save_data = false;
bool retrieve_data = false;

bool fsr1tone = false;
bool fsr2tone = false;
bool acctone = false;

bool devPlot1 = true;
bool devPlot2 = true;
bool devPlot3 = true;

bool device4s = false;
//CGContextRef drawingContext;

NSTimer *plotTimer;


- (IBAction)saveInfo:(id)sender {
    if (!save_data) {
        save_data = true;
        [saveInfo setTitle:@ "Stop" forState:UIControlStateNormal];
        
        if(plotTimer)
        {
            [plotTimer invalidate];
            plotTimer = nil;
        }
        
        [self startTimer];
        
        NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES)objectAtIndex:0];
        //        NSString *fileTest=[docPath stringByAppendingPathComponent:@"results.csv"];
        NSString *fileTest=[docPath stringByAppendingPathComponent:@"ztmp.csv"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:fileTest])
            deleteFileAtPath:fileTest;
        
        [[NSFileManager defaultManager]
         createFileAtPath:fileTest contents:nil attributes:nil];
        
        
        
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:fileTest];
        [fileHandle closeFile];
    }
    else {
        save_data = false;
        [saveInfo setTitle:@ "Capture" forState:UIControlStateNormal];
        
    }
}

- (IBAction)retrieveInfo:(id)sender {
    if (!retrieve_data){
        
        _chart1.hidden = true;
        
        retrieve_data = true;
        save_data = false;
        [saveInfo setTitle:@ "Capture" forState:UIControlStateNormal];
        resultsView.hidden = NO;
        [retrieveInfo setTitle:@ "Plot" forState:UIControlStateNormal];//"Hide"
        NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES)objectAtIndex:0];
        //        NSString *fileTest=[docPath stringByAppendingPathComponent:@"results.csv"];
        NSString *fileTest=[docPath stringByAppendingPathComponent:@"ztmp.csv"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath: fileTest]) {
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:fileTest];
            NSString *fileTestResults=[[NSString alloc]initWithData:[fileHandle availableData] encoding:NSUTF8StringEncoding];
            [fileHandle closeFile];
            self.resultsView.text=fileTestResults;
            NSLog(@"info retrieved");
        }
        else
            NSLog(@"info not retrieved");
    }
    else {
        retrieve_data = false;
        resultsView.hidden = YES;
        [retrieveInfo setTitle:@ "CSV" forState:UIControlStateNormal];//"View"
        
        _chart1.hidden = false;
        
        
    }
    
    [self loadChartData];
    _chart1.datasource = self;
    _chart1.autoCalculateAxisRanges = YES;
    //    [self->_chart1 redrawChart];
    [self->_chart1 redrawChartIncludePlotArea:(YES)];
    
}


- (IBAction)eMailInfo:(id)sender {
    
    [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"I commented out all the code for this funcationality." delegate:nil cancelButtonTitle:@"OKay Thanks!" otherButtonTitles:nil] show];
    
//    save_data = false;
//    
//    NSString *tmpFileName = openFileName;
//    NSString *hwString = openFileName;
//    NSString *hString = hwString;
//    NSString *tmpFileName2 = [hString stringByAppendingString:@".csv"];
//    NSString *tmpFileName3 = [hString stringByAppendingString:@".pdf"];
//    
//    
//    if ([MFMailComposeViewController canSendMail]){
//        NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES)objectAtIndex:0];
//        NSString *fileTest=[docPath stringByAppendingPathComponent:@"ztmp.csv"];
//        
//        
//        NSString *PDFfileName=[docPath stringByAppendingPathComponent:@"ztmp.pdf"];
//        [self generatePDF:PDFfileName];
//        
//        
//        if ([[NSFileManager defaultManager] fileExistsAtPath: fileTest]) {
//            
//            
//            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
//            mailer.mailComposeDelegate = self;
//            //            [mailer setSubject:@"PDF File"];
//            [mailer addAttachmentData:[NSData dataWithContentsOfFile:PDFfileName]
//                             mimeType:@"application/pdf"
//                             fileName:tmpFileName3];
//            
//            //            [mailer setSubject:@"CSV File"];
//            [mailer setSubject:@"PDF & CSV Files"];
//            [mailer addAttachmentData:[NSData dataWithContentsOfFile:fileTest]
//                             mimeType:@"text/csv"
//                             fileName:tmpFileName2];
//            
//            [self presentModalViewController:mailer animated:YES];
//            
//            NSLog(@"info retrieved");
//            
//            NSFileManager *fileManager = [NSFileManager defaultManager];
//            [fileManager removeItemAtPath:PDFfileName error:NULL];
//            
//        }
//        else {
//            NSLog(@"info not retrieved");
//        
//            NSFileManager *fileManager = [NSFileManager defaultManager];
//            [fileManager removeItemAtPath:PDFfileName error:NULL];
//        }
//    }
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error

{
    
    switch (result)
    
    {
            
        case MFMailComposeResultCancelled:
            
            NSLog(@"Mail cancelled");
            
            break;
            
        case MFMailComposeResultSaved:
            
            NSLog(@"Mail saved");
            
            break;
            
        case MFMailComposeResultSent:
            
            NSLog(@"Mail sent");
            
            break;
            
        case MFMailComposeResultFailed:
            
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            
            break;
            
        default:
            
            break;
            
    }
    
    // Close the Mail Interface
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void) startTimer {
    plotTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(tick:) //0.0333
                                               userInfo:nil repeats:YES];
}

- (void) tick:(NSTimer *) timer {
    
    if (save_data)
    {
        [self.testValues addObject:@[@(Final1X), @(Final1Y), @(Final1Z), @(Final14), @(Final2X), @(Final2Y), @(Final2Z), @(Final24), @(Final3X), @(Final3Y), @(Final3Z)]];
        
        NSLog(@"info saved");
    }
    
}

- (IBAction)switchPlotSwitch {
    
    if (dev1PlotSwitch.on) {
        devPlot1 = true;
        [self loadChartData];
        _chart1.datasource = self;
        _chart1.autoCalculateAxisRanges = YES;
        //        [self->_chart1 redrawChart];
        [self->_chart1 redrawChartIncludePlotArea:(YES)];
    }
    else {
        devPlot1 = false;
        [self loadChartData];
        _chart1.datasource = self;
        _chart1.autoCalculateAxisRanges = YES;
        //        [self->_chart1 redrawChart];
        [self->_chart1 redrawChartIncludePlotArea:(YES)];
    }
    
    if (dev2PlotSwitch.on) {
        devPlot2 = true;
        [self loadChartData];
        _chart1.datasource = self;
        _chart1.autoCalculateAxisRanges = YES;
        //        [self->_chart1 redrawChart];
        [self->_chart1 redrawChartIncludePlotArea:(YES)];
    }
    else {
        devPlot2 = false;
        [self loadChartData];
        _chart1.datasource = self;
        _chart1.autoCalculateAxisRanges = YES;
        //        [self->_chart1 redrawChart];
        [self->_chart1 redrawChartIncludePlotArea:(YES)];
    }
    
    if (dev3PlotSwitch.on) {
        devPlot3 = true;
        [self loadChartData];
        _chart1.datasource = self;
        _chart1.autoCalculateAxisRanges = YES;
        //        [self->_chart1 redrawChart];
        [self->_chart1 redrawChartIncludePlotArea:(YES)];
    }
    else {
        devPlot3 = false;
        [self loadChartData];
        _chart1.datasource = self;
        _chart1.autoCalculateAxisRanges = YES;
        //        [self->_chart1 redrawChart];
        [self->_chart1 redrawChartIncludePlotArea:(YES)];
    }
    
}


-(IBAction)fileButton
{
    fileView.hidden = NO;
    file2View.hidden = NO;
    
    TargetSelectedButton.enabled = false;
    ShoesButton.enabled = false;
    GridButton.enabled = false;
    AccButton.enabled = false;
    AlarmButton.enabled = false;
    LimitsButton.enabled = false;
    DisplayIndicatorButton.enabled = false;
    ChangeColor1Button.enabled = false;
    ChangeColor2Button.enabled = false;
    ChangeColor3Button.enabled = false;
    ChangeColor4Button.enabled = false;
    TSUIScanForPeripheralsButton.enabled = false;
    TSUIScanForPeripheralsButtonL.enabled = false;
    TSUIScanForPeripheralsButtonAcc.enabled = false;
    
    RawButton.enabled = false;
    ZeroFSRButton.enabled = false;
    
    filenames = [self getAllPatientTests];
    
    [tableView reloadData];
}

- (NSArray *)getAllPatientTests
{
    return [Test fetchWithPredicate:[NSPredicate predicateWithFormat:@"testPatient = %@", self.patient] sortDescriptor:@[[NSSortDescriptor sortDescriptorWithKey:@"testCreatedAt" ascending:NO]] fetchLimit:0];
}


-(IBAction)FVcancelButton {
    fileView.hidden = YES;
    file2View.hidden = YES;
    
    TargetSelectedButton.enabled = true;
    ShoesButton.enabled = true;
    GridButton.enabled = true;
    AccButton.enabled = true;
    AlarmButton.enabled = true;
    LimitsButton.enabled = true;
    DisplayIndicatorButton.enabled = true;
    ChangeColor1Button.enabled = true;
    ChangeColor2Button.enabled = true;
    ChangeColor3Button.enabled = true;
    ChangeColor4Button.enabled = true;
    TSUIScanForPeripheralsButton.enabled = true;
    TSUIScanForPeripheralsButtonL.enabled = true;
    TSUIScanForPeripheralsButtonAcc.enabled = true;
    
    RawButton.enabled = true;
    ZeroFSRButton.enabled = true;
    
    if (PlotSelected) {
        FVsaveButton.hidden = NO;
        if (retrieve_data)
            _chart1.hidden = YES;
        else
            _chart1.hidden = NO;
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSLog(@"didSelectRowAtIndexPath");
    
    openTestIndex = indexPath.row;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return indexPath;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filenames count];
}



- (UITableViewCell *)tableView:(UITableView *)tblView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    Test *test = filenames[indexPath.row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyy HH:mm:ss";
    
    cell.textLabel.text = [NSString stringWithFormat:@"%ld: %@", (long)indexPath.row, [dateFormatter stringFromDate:test.testCreatedAt]];
    
    return cell;
}



-(IBAction)FVopenButton {

    if (openTestIndex != -1)
    {
        Test *patientTest = filenames[openTestIndex];
        
        rowArray = [[NSArray alloc] init];
        rowArray = [NSKeyedUnarchiver unarchiveObjectWithData:patientTest.testData];
        
        float flfsr1;
        float flfsr2;
        float flfsr3;
        float flfsr4;
        float fl2fsr1;
        float fl2fsr2;
        float fl2fsr3;
        float fl2fsr4;
        //        float fl3fsr1;
        //        float fl3fsr2;
        //        float fl3fsr3;
        
        //        int testdat;
        
        firstFSRArray = [[NSMutableArray alloc]init];
        secondFSRArray = [[NSMutableArray alloc]init];
        thirdFSRArray = [[NSMutableArray alloc]init];
        fourthFSRArray = [[NSMutableArray alloc]init];
        fifthFSRArray = [[NSMutableArray alloc]init];
        sixthFSRArray = [[NSMutableArray alloc]init];
        seventhFSRArray = [[NSMutableArray alloc]init];
        eighthFSRArray = [[NSMutableArray alloc]init];
        ninthFSRArray = [[NSMutableArray alloc]init];
        tenthFSRArray = [[NSMutableArray alloc]init];
        eleventhFSRArray = [[NSMutableArray alloc]init];
        
        StartDraw = 1;
        StopDraw = 0;
        
        
        for(int i = 0; i < ([rowArray count] - 1); i++)
        {
            
            NSArray *tempArray = [rowArray objectAtIndex:i];
            
            [firstFSRArray addObject:[tempArray objectAtIndex:0]];
            [secondFSRArray addObject:[tempArray objectAtIndex:1]];
            [thirdFSRArray addObject:[tempArray objectAtIndex:2]];
            [fourthFSRArray addObject:[tempArray objectAtIndex:3]];
            [fifthFSRArray addObject:[tempArray objectAtIndex:4]];
            [sixthFSRArray addObject:[tempArray objectAtIndex:5]];
            [seventhFSRArray addObject:[tempArray objectAtIndex:6]];
            [eighthFSRArray addObject:[tempArray objectAtIndex:7]];
            [ninthFSRArray addObject:[tempArray objectAtIndex:8]];
            [tenthFSRArray addObject:[tempArray objectAtIndex:9]];
            [eleventhFSRArray addObject:[tempArray objectAtIndex:10]];
            
            
            
            // device 1
            
            //            testdat = tempArray[0];
            
            flfsr1 = [tempArray[0] floatValue];
            flfsr2 = [tempArray[1] floatValue];
            flfsr3 = [tempArray[2] floatValue];
            
            fl2fsr1 = [tempArray[4] floatValue];
            fl2fsr2 = [tempArray[5] floatValue];
            fl2fsr3 = [tempArray[6] floatValue];
            
            //            flfsr1 = firstFSRArray[i];
            //            flfsr2 = secondFSRArray[i];
            //            flfsr3 = thirdFSRArray[i];
            //            fl2fsr1 = fourthFSRArray[i];
            //            fl2fsr2 = fifthFSRArray[i];
            //            fl2fsr3 = sixthFSRArray[i];
            
            LC2 = flfsr1 + flfsr2;
            LC4 = flfsr3;
            LC1_2 = LC1 + LC2;
            LC3_4 = LC3 + LC4;
            LC1_3 = LC1 + LC3;
            LC2_4 = LC2 + LC4;
            
            if (LC1_2 == LC3_4) tverticalpos = 50;
            else {
                if (LC1_2 > LC3_4) tverticalpos = (((LC3_4 / LC1_2) / 2) * 100);
                else tverticalpos = ((((1 - (LC1_2 / LC3_4)) / 2) + 0.5) * 100);
            }
            if (LC1_3 == LC2_4) thorizontalpos = 50;
            else {
                if (LC1_3 > LC2_4) thorizontalpos = (((LC2_4 / LC1_3) / 2) * 100);
                else thorizontalpos = ((((1 - (LC1_3 / LC2_4)) / 2) + 0.5) * 100);
            }
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                horizontalpos=(thorizontalpos * 5);
                verticalpos=(tverticalpos * 2.5);
            }
            else { //iPhone
                horizontalpos=(thorizontalpos * 2);
                verticalpos=(tverticalpos * 1.6);
            }
            
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //                [self->graphview setNeedsDisplay];
            //            });
            //[self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:self waitUntilDone:TRUE];
            //            [self->graphview setNeedsDisplay];
            //            [self->graphview drawRect:(CGRect)rect];
            //            [graphview updateData];
            //            Draw *Draw = [[Draw alloc]init];
            //            [Draw drawRect];
            //            [Draw release];
            
            // device 2
            
            LC1 = fl2fsr1 + fl2fsr2;
            LC3 = fl2fsr3;
            LC1_2 = LC1 + LC2;
            LC3_4 = LC3 + LC4;
            LC1_3 = LC1 + LC3;
            LC2_4 = LC2 + LC4;
            
            if (LC1_2 == LC3_4) tverticalpos = 50;
            else {
                if (LC1_2 > LC3_4) tverticalpos = (((LC3_4 / LC1_2) / 2) * 100);
                else tverticalpos = ((((1 - (LC1_2 / LC3_4)) / 2) + 0.5) * 100);
            }
            if (LC1_3 == LC2_4) thorizontalpos = 50;
            else {
                if (LC1_3 > LC2_4) thorizontalpos = (((LC2_4 / LC1_3) / 2) * 100);
                else thorizontalpos = ((((1 - (LC1_3 / LC2_4)) / 2) + 0.5) * 100);
            }
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                horizontalpos=(thorizontalpos * 5);
                verticalpos=(tverticalpos * 2.5);
            }
            else { //iPhone
                horizontalpos=(thorizontalpos * 2);
                verticalpos=(tverticalpos * 1.6);
            }
            
            [self->graphview setNeedsDisplay];
            
        }
        StartDraw = 0;
        StopDraw = 1;
        
        fileView.hidden = YES;
        file2View.hidden = YES;
        
        TargetSelectedButton.enabled = true;
        ShoesButton.enabled = true;
        GridButton.enabled = true;
        AccButton.enabled = true;
        AlarmButton.enabled = true;
        LimitsButton.enabled = true;
        DisplayIndicatorButton.enabled = true;
        ChangeColor1Button.enabled = true;
        ChangeColor2Button.enabled = true;
        ChangeColor3Button.enabled = true;
        ChangeColor4Button.enabled = true;
        TSUIScanForPeripheralsButton.enabled = true;
        TSUIScanForPeripheralsButtonL.enabled = true;
        TSUIScanForPeripheralsButtonAcc.enabled = true;
        
        RawButton.enabled = true;
        ZeroFSRButton.enabled = true;
        
        FVsaveButton.hidden = NO;
        if (retrieve_data)
            _chart1.hidden = YES;
        else
            _chart1.hidden = NO;
        
        [self loadChartData];
        _chart1.datasource = self;
        _chart1.autoCalculateAxisRanges = YES;
        

        [self->_chart1 redrawChartIncludePlotArea:(YES)];

        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        dateFormater.dateFormat = @"dd/MM/yyyy HH:mm:ss";
        
        _chart1.title = [NSString stringWithFormat:@"%ld: %@", (long)openTestIndex, [dateFormater stringFromDate:patientTest.testCreatedAt]];
    }
}


-(IBAction)FVsaveButton
{
//    fileView.hidden = NO;
//    file2View.hidden = YES;
//    fileSaveView.hidden = NO;
//
//
    [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Test records are already saved in Core-Data. No need to saved them anywhere else" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil] show];
}


-(IBAction)FVdeleteButton
{
    if (openTestIndex != -1)
    {
        Test *testToDelete = filenames[openTestIndex];
        [Test deleteObject:testToDelete];
        
        NSLog(@"Test deleted");
        
        _chart1.title = @"Results";

        openTestIndex = -1;
        
        filenames = [self getAllPatientTests];
        
        [tableView reloadData];
    }
}


-(IBAction)FSsaveButton {
    
//    NSString *tmpFileName = self.textFieldText.text;
//    
//    openFileName = self.textFieldText.text;
//    
//    NSData *tmpData;
//    
//    NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES)objectAtIndex:0];
//    NSString *fileTest=[docPath stringByAppendingPathComponent:@"ztmp.csv"];
//    
//    if ([[NSFileManager defaultManager] fileExistsAtPath: fileTest]) {
//        NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:fileTest];
//        
//        tmpData=[fileHandle availableData];
//        [fileHandle closeFile];
//        
//    }
//    
//    NSString *fileTest2=[docPath stringByAppendingPathComponent:tmpFileName];
//    
//    if ([[NSFileManager defaultManager] fileExistsAtPath:fileTest])
//        deleteFileAtPath:fileTest2;
//    
//    [[NSFileManager defaultManager]
//     createFileAtPath:fileTest2 contents:nil attributes:nil];
//    
//    
//    
//    NSFileHandle *fileHandle2 = [NSFileHandle fileHandleForUpdatingAtPath:fileTest2];
//    
//    [fileHandle2 writeData:tmpData];
//    [fileHandle2 closeFile];
//    NSLog(@"info saved");
//    
//    fileView.hidden = YES;
//    file2View.hidden = YES;
//    fileSaveView.hidden = YES;
//    
//    [textField resignFirstResponder];
//    
//    _chart1.title = tmpFileName;
//    
//    if (PlotSelected) {
//        FVsaveButton.hidden = NO;
//        if (retrieve_data)
//            _chart1.hidden = YES;
//        else
//            _chart1.hidden = NO;
//        
//    }else {
//        TargetSelectedButton.enabled = true;
//        ShoesButton.enabled = true;
//        GridButton.enabled = true;
//        AccButton.enabled = true;
//        AlarmButton.enabled = true;
//        LimitsButton.enabled = true;
//        DisplayIndicatorButton.enabled = true;
//        ChangeColor1Button.enabled = true;
//        ChangeColor2Button.enabled = true;
//        ChangeColor3Button.enabled = true;
//        ChangeColor4Button.enabled = true;
//        TSUIScanForPeripheralsButton.enabled = true;
//        TSUIScanForPeripheralsButtonL.enabled = true;
//        TSUIScanForPeripheralsButtonAcc.enabled = true;
//        
//        RawButton.enabled = true;
//        ZeroFSRButton.enabled = true;
//    }
//    [tableView reloadData];
//    
}


-(IBAction)FScancelButton {
    fileSaveView.hidden = YES;
    
    [textField resignFirstResponder];
    
    if (PlotSelected) {
        FVsaveButton.hidden = NO;
        fileView.hidden = YES;
        if (retrieve_data)
            _chart1.hidden = YES;
        else
            _chart1.hidden = NO;
        
    }else {
        fileView.hidden = NO;
        file2View.hidden = NO;
        
    }
}


-(IBAction)PlotRecallButton {
    
    fileView.hidden = NO;
    file2View.hidden = NO;
    
    openTestIndex = -1;
    
    _chart1.hidden = YES;
    FVsaveButton.hidden = YES;
    
    NSString *resPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES)objectAtIndex:0];
    NSError *error = nil;
    filenames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:resPath error:&error];
    if (!error)
    {
        for (NSString * filename in filenames)
        {
            NSString *extension = [filename pathExtension];
            if ([extension isEqualToString:@"csv"])
            {
                NSLog(@"%@", filename);
            }
        }
    }
    
    [tableView reloadData];
    
}


-(IBAction)PlotSaveButton {
    fileView.hidden = NO;
    file2View.hidden = YES;
    fileSaveView.hidden = NO;
    _chart1.hidden = YES;
    
}


-(IBAction)plotButton {
    plotView.hidden = NO;
    PlotSelected = YES;
    resultsView.hidden = YES;
    
    feetButton.hidden = YES;
    balanceButton.hidden = YES;

    
    //    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { //iPhone
    //        RawButton.hidden = true;
    //        ZeroFSRButton.hidden = true;
    //    }
    setupView.hidden = false;
    
    TargetSelectedButton.hidden = true;
    ShoesButton.hidden = true;
    GridButton.hidden = true;
    AccButton.hidden = true;
    AlarmButton.hidden = true;
    LimitsButton.hidden = true;
    DisplayIndicatorButton.hidden = true;
    ChangeColor1Button.hidden = true;
    ChangeColor2Button.hidden = true;
    ChangeColor3Button.hidden = true;
    ChangeColor4Button.hidden = true;
    TSUIScanForPeripheralsButton.hidden = true;
    TSUIScanForPeripheralsButtonL.hidden = true;
    TSUIScanForPeripheralsButtonAcc.hidden = true;
    
    RawButton.hidden = true;
    ZeroFSRButton.hidden = true;
    
    
    
    [self loadChartData];
    
    [self.view addSubview:_chart1];
    _chart1.datasource = self;
    _chart1.hidden = false;
    _chart1.autoCalculateAxisRanges = YES;
    
    [self->_chart1 redrawChartIncludePlotArea:(YES)];
    
    /*    [self.view addSubview:_chart2];
     _chart2.datasource = self;
     _chart2.hidden = false;
     
     [self.view addSubview:_chart3];
     _chart3.datasource = self;
     _chart3.hidden = false;*/
    
    closePlotButton.hidden = false;
    dev1PlotSwitch.hidden = false;
    dev2PlotSwitch.hidden = false;
//    dev3PlotSwitch.hidden = false;
    
    [retrieveInfo setTitle:@ "CSV" forState:UIControlStateNormal];//"View"
    resultsView.hidden = true;
    retrieve_data = false;
    
}

-(IBAction)closePlotButton{
    plotView.hidden = true;
    PlotSelected = false;
    
    feetButton.hidden = NO;
    balanceButton.hidden = NO;
    
    //    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { //iPhone
    //        RawButton.hidden = false;
    //        ZeroFSRButton.hidden = false;
    //    }
    setupView.hidden = true;
    
    _chart1.hidden = true;
    //    _chart2.hidden = true;
    //    _chart3.hidden = true;
    closePlotButton.hidden = true;
    dev1PlotSwitch.hidden = true;
    dev2PlotSwitch.hidden = true;
    dev3PlotSwitch.hidden = true;
    
    
    TargetSelectedButton.hidden = false;
    ShoesButton.hidden = false;
    GridButton.hidden = false;
//    AccButton.hidden = false;
    AlarmButton.hidden = false;
    LimitsButton.hidden = false;
    DisplayIndicatorButton.hidden = false;
    if (BalanceMode){
        ChangeColor1Button.hidden = false;
        ChangeColor2Button.hidden = false;
        ChangeColor3Button.hidden = false;
        ChangeColor4Button.hidden = false;
    }
    TSUIScanForPeripheralsButton.hidden = false;
    TSUIScanForPeripheralsButtonL.hidden = false;
    
    if (AccSelected){
        TSUIScanForPeripheralsButtonAcc.hidden = false;
    }
    else {
        TSUIScanForPeripheralsButtonAcc.hidden = true;
    }
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
        RawButton.hidden = false;
        ZeroFSRButton.hidden = false;
    }
    else { //iPhone
        RawButton.hidden = true;
        ZeroFSRButton.hidden = true;
    }
    
    
}


-(IBAction)dispModeButton {
    if (dispMode == 0) {
        leftFoot.hidden = false;
        rightFoot.hidden = false;
        
        if (AccSelected){
            puttS.hidden = true;
            puttT.hidden = true;
            labelAP.hidden = false;
            labelLateral.hidden = false;
            labelVertical.hidden = false;
        }
        
        UIImage *image2 = [UIImage imageNamed:@"grid2.png"];
        [grid setImage:image2];
        grid.alpha = 0.6;
        
        label3.textColor = [UIColor darkGrayColor];
        labelL3.textColor = [UIColor darkGrayColor];
        
        label7.textColor = [UIColor blueColor];
        label20.textColor = [UIColor blueColor];
        label40.textColor = [UIColor blueColor];
        label46.textColor = [UIColor blueColor];
        label6.textColor = [UIColor blueColor];
        label18.textColor = [UIColor blueColor];
        label41.textColor = [UIColor blueColor];
        label47.textColor = [UIColor blueColor];
        label10.textColor = [UIColor blueColor];
        label17.textColor = [UIColor blueColor];
        label42.textColor = [UIColor blueColor];
        label48.textColor = [UIColor blueColor];
        label11.textColor = [UIColor blueColor];
        label19.textColor = [UIColor blueColor];
        label43.textColor = [UIColor blueColor];
        label49.textColor = [UIColor blueColor];
        label44.textColor = [UIColor blueColor];
        label50.textColor = [UIColor blueColor];
        label45.textColor = [UIColor blueColor];
        label51.textColor = [UIColor blueColor];
        
        dispMode = 1;
        
        [[NSUserDefaults standardUserDefaults] setInteger:dispMode forKey:@"dispModeVal"];
        
        [dispModeButton setTitle:@ "General" forState:UIControlStateNormal];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
            UIImage *image = [UIImage imageNamed:@"grass_02.png"];
            [basePlate setImage:image];
            basePlate.frame = CGRectMake(0, 100-iOS_offset, 768, 529); //80 -> 100
            label3.center = CGPointMake(557, 525-iOS_offset); //465 -> 485
            labelL3.center = CGPointMake(212, 525-iOS_offset);
            golfShoes.hidden = true;
            
            UIImage *image3 = [UIImage imageNamed:@"grass_02.png"];
            [accelerometer setImage:image3];
            accelerometer.frame = CGRectMake(0, 100-iOS_offset, 768, 529);
        }
        else { //iPhone
            golfShoeL.hidden = true;
            golfShoeR.hidden = true;
            UIImage *image = [UIImage imageNamed:@"grass_04.png"];
            [basePlate setImage:image];
            basePlate.frame = CGRectMake(0, 71-iOS_offset, 320, 315);
            
            UIImage *image3 = [UIImage imageNamed:@"grass_04.png"];
            [accelerometer setImage:image3];
            accelerometer.frame = CGRectMake(0, 71-iOS_offset, 320, 315);
            
            label13.center = CGPointMake(14, 179-iOS_offset);
            label12.center = CGPointMake(8, 311-iOS_offset);
            label14.center = CGPointMake(269, 311-iOS_offset);
            label28.center = CGPointMake(56, 179-iOS_offset);
            label27.center = CGPointMake(51, 311-iOS_offset);
            label29.center = CGPointMake(313, 311-iOS_offset);
            arrow2.center = CGPointMake(245, 255-iOS_offset);
            arrow4.center = CGPointMake(100, 255-iOS_offset);
            acchorizontals.center = CGPointMake(100, 179-iOS_offset);
            accverticals.center = CGPointMake(24, 257-iOS_offset);
            acc45degs.center = CGPointMake(289, 257-iOS_offset);
            accvertical.center = CGPointMake(100, 255-iOS_offset);
            acc45deg.center = CGPointMake(245, 255-iOS_offset);
            acchorizontal.center = CGPointMake(100, 255-iOS_offset);
        }
    }
    else {
        leftFoot.hidden = true;
        rightFoot.hidden = true;
        
        if (AccSelected){
            puttS.hidden = false;
            puttT.hidden = false;
            labelAP.hidden = true;
            labelLateral.hidden = true;
            labelVertical.hidden = true;
        }
        
        UIImage *image2 = [UIImage imageNamed:@"grid.png"];
        [grid setImage:image2];
        grid.alpha = 1.0;
        
        label3.textColor = [UIColor lightGrayColor];
        labelL3.textColor = [UIColor lightGrayColor];
        
        label7.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label20.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label40.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label46.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label6.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label18.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label41.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label47.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label10.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label17.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label42.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label48.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label11.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label19.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label43.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label49.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label44.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label50.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label45.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        label51.textColor = [UIColor colorWithRed:(0.0/255.0) green:(153.0/255.0) blue:(255.0/255.0) alpha:1.0];
        
        dispMode = 0;
        
        [[NSUserDefaults standardUserDefaults] setInteger:dispMode forKey:@"dispModeVal"];
        
        [dispModeButton setTitle:@ "Shoes" forState:UIControlStateNormal];
        
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
            UIImage *image = [UIImage imageNamed:@"grass_01.png"];
            [basePlate setImage:image];
            basePlate.frame = CGRectMake(0, 80-iOS_offset, 768, 569); //60 -> 80
            label3.center = CGPointMake(547, 525-iOS_offset); //465 -> 485
            labelL3.center = CGPointMake(226, 525-iOS_offset);
            golfShoes.hidden = false;
            
            UIImage *image3 = [UIImage imageNamed:@"grass_01.png"];
            [accelerometer setImage:image3];
            accelerometer.frame = CGRectMake(0, 80-iOS_offset, 768, 569);
        }
        else { //iPhone
            golfShoeL.hidden = false;
            golfShoeR.hidden = false;
            UIImage *image = [UIImage imageNamed:@"grass_iPhone5.png"];
            [basePlate setImage:image];
            basePlate.frame = CGRectMake(0, 64-iOS_offset, 320, 330);
            
            UIImage *image3 = [UIImage imageNamed:@"grass_iPhone5.png"];
            [accelerometer setImage:image3];
            accelerometer.frame = CGRectMake(0, 64-iOS_offset, 320, 330);
            
            label13.center = CGPointMake(8, 238-iOS_offset);
            label12.center = CGPointMake(2, 370-iOS_offset);
            label14.center = CGPointMake(264, 370-iOS_offset);
            label28.center = CGPointMake(50, 238-iOS_offset);
            label27.center = CGPointMake(45, 370-iOS_offset);
            label29.center = CGPointMake(307, 370-iOS_offset);
            arrow2.center = CGPointMake(240, 314-iOS_offset);
            arrow4.center = CGPointMake(94, 314-iOS_offset);
            acchorizontals.center = CGPointMake(94, 238-iOS_offset);
            accverticals.center = CGPointMake(18, 316-iOS_offset);
            acc45degs.center = CGPointMake(284, 316-iOS_offset);
            accvertical.center = CGPointMake(95, 314-iOS_offset);
            acc45deg.center = CGPointMake(240, 314-iOS_offset);
            acchorizontal.center = CGPointMake(95, 314-iOS_offset);
        }
    }
}

-(IBAction)StartDrawButton
{
    
    StartDraw = 1;
    StopDraw = 0;
    ClearDraw = 0;

    StartDrawButton.hidden = YES;
    StopDrawButton.hidden = NO;
    
    save_data = true;
    
    if(plotTimer)
    {
        [plotTimer invalidate];
        plotTimer = nil;
    }
    
    [self.testValues removeAllObjects];
    
    _chart1.title = @"Results";

    [self startTimer];
}

-(IBAction)StopDrawButton
{
    
    StartDraw = 0;
    StopDraw = 1;
    ClearDraw = 0;
    StopDrawButton.hidden = YES;
    StartDrawButton.hidden = NO;

    save_data = false;
    
    // Save Array data into CoreData
    Test *newTest = (Test *)[Test create];

    newTest.testPatient = self.patient;
    newTest.testCreatedAt = [NSDate date];
    newTest.testId = @([Test count] + 1);
    newTest.testData = [NSKeyedArchiver archivedDataWithRootObject:self.testValues];
    
    [Test save];
    
    
    // Remove all the data when saved in database
    [self.testValues removeAllObjects];
}

-(IBAction)balanceButton {
    BalanceMode = YES;
    
    UIImage *feetButtonPressed = [UIImage imageNamed:@"balance_btn_up.png"];
    [feetButton setImage:feetButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:feetButton];
    
    UIImage *balanceButtonPressed = [UIImage imageNamed:@"feet_btn.png"];
    [balanceButton setImage:balanceButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:balanceButton];
    
    basePlate.hidden = NO;
    graphview.hidden = NO;
    ChangeColor1Button.hidden = NO;
    ChangeColor2Button.hidden = NO;
    ChangeColor3Button.hidden = NO;
    ChangeColor4Button.hidden = NO;
    ClearDrawButton.hidden = NO;
    balanceView.hidden = NO;
    balanceTracking.hidden = NO;
    
    TargetSelectedButton.hidden = true;
    ShoesButton.hidden = true;
    GridButton.hidden = true;
//    AccButton.hidden = true;
    AlarmButton.hidden = true;
    LimitsButton.hidden = true;
    DisplayIndicatorButton.hidden = true;
    RawButton.hidden = true;
    ZeroFSRButton.hidden = true;

    los_1p = 0;
    los_2p = 0;
    los_3p = 0;
    los_4p = 0;
    los_5p = 0;
    los_6p = 0;
    los_7p = 0;
    los_8p = 0;
    los1.text = [NSString stringWithFormat:@"%.1f", los_1p];
    los2.text = [NSString stringWithFormat:@"%.1f", los_2p];
    los3.text = [NSString stringWithFormat:@"%.1f", los_3p];
    los4.text = [NSString stringWithFormat:@"%.1f", los_4p];
    los5.text = [NSString stringWithFormat:@"%.1f", los_5p];
    los6.text = [NSString stringWithFormat:@"%.1f", los_6p];
    los7.text = [NSString stringWithFormat:@"%.1f", los_7p];
    los8.text = [NSString stringWithFormat:@"%.1f", los_8p];

    los1.hidden = NO;
//    los2.hidden = NO;
    los3.hidden = NO;
//    los4.hidden = NO;
    los5.hidden = NO;
//    los6.hidden = NO;
    los7.hidden = NO;
//    los8.hidden = NO;
    los1in.hidden = NO;
//    los2in.hidden = NO;
    los3in.hidden = NO;
//    los4in.hidden = NO;
    los5in.hidden = NO;
//    los6in.hidden = NO;
    los7in.hidden = NO;
//    los8in.hidden = NO;
    
}

-(IBAction)feetButton {
    BalanceMode = NO;
    
    UIImage *feetButtonPressed = [UIImage imageNamed:@"balance_btn.png"];
    [feetButton setImage:feetButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:feetButton];
    
    UIImage *balanceButtonPressed = [UIImage imageNamed:@"feet_btn_up.png"];
    [balanceButton setImage:balanceButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:balanceButton];
    
    
    basePlate.hidden = YES;
    graphview.hidden = YES;
    ChangeColor1Button.hidden = YES;
    ChangeColor2Button.hidden = YES;
    ChangeColor3Button.hidden = YES;
    ChangeColor4Button.hidden = YES;
    ClearDrawButton.hidden = YES;
    balanceView.hidden = YES;
    balanceTracking.hidden = YES;
    
    TargetSelectedButton.hidden = false;
    ShoesButton.hidden = false;
    GridButton.hidden = false;
    //    AccButton.hidden = false;
    AlarmButton.hidden = false;
    LimitsButton.hidden = false;
    DisplayIndicatorButton.hidden = false;
    RawButton.hidden = false;
    ZeroFSRButton.hidden = false;
    
    los1.hidden = YES;
    los2.hidden = YES;
    los3.hidden = YES;
    los4.hidden = YES;
    los5.hidden = YES;
    los6.hidden = YES;
    los7.hidden = YES;
    los8.hidden = YES;
    los1in.hidden = YES;
    los2in.hidden = YES;
    los3in.hidden = YES;
    los4in.hidden = YES;
    los5in.hidden = YES;
    los6in.hidden = YES;
    los7in.hidden = YES;
    los8in.hidden = YES;


}

-(IBAction)ClearDrawButton {
    //    StartDraw = 0;
    //    StopDraw = 0;
    ClearDraw = 1;
    [self->graphview setNeedsDisplay];
    GraphOnly = false;
    AlarmLine = 0;
    los_1p = 0;
    los_2p = 0;
    los_3p = 0;
    los_4p = 0;
    los_5p = 0;
    los_6p = 0;
    los_7p = 0;
    los_8p = 0;
    los1.text = [NSString stringWithFormat:@"%.1f", los_1p];
    los2.text = [NSString stringWithFormat:@"%.1f", los_2p];
    los3.text = [NSString stringWithFormat:@"%.1f", los_3p];
    los4.text = [NSString stringWithFormat:@"%.1f", los_4p];
    los5.text = [NSString stringWithFormat:@"%.1f", los_5p];
    los6.text = [NSString stringWithFormat:@"%.1f", los_6p];
    los7.text = [NSString stringWithFormat:@"%.1f", los_7p];
    los8.text = [NSString stringWithFormat:@"%.1f", los_8p];
    
    if (save_data) {
        if(plotTimer)
        {
            [plotTimer invalidate];
            plotTimer = nil;
        }
        
        [self startTimer];
        
        NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES)objectAtIndex:0];
        //        NSString *fileTest=[docPath stringByAppendingPathComponent:@"results.csv"];
        NSString *fileTest=[docPath stringByAppendingPathComponent:@"ztmp.csv"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:fileTest])
            deleteFileAtPath:fileTest;
        
        [[NSFileManager defaultManager]
         createFileAtPath:fileTest contents:nil attributes:nil];
        
        
        
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:fileTest];
        [fileHandle closeFile];
    }
}

-(IBAction)AlarmButton {
    if (AlarmSelected){
        UIImage *AlarmButtonPressed = [UIImage imageNamed:@"Alarm-Up.png"];
        [AlarmButton setImage:AlarmButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:AlarmButton];
        AlarmSelected = false;
        playButton.alpha = 1.0;
        playButton.enabled = true;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"AlarmSelVal"];
    }
    else{
        playButton.alpha = 0.5;
        playButton.enabled = false;
        UIImage *AlarmButtonPressed = [UIImage imageNamed:@"Alarm-Selected.png"];
        [AlarmButton setImage:AlarmButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:AlarmButton];
        AlarmSelected = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"AlarmSelVal"];
    }
}

-(IBAction)GridButton {
    if (GridSelected){
        grid.hidden = YES;
        UIImage *GridButtonPressed = [UIImage imageNamed:@"Grid-Up.png"];
        [GridButton setImage:GridButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:GridButton];
        GridSelected = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"GridSelVal"];
    }
    else{
        grid.hidden = NO;
        UIImage *GridButtonPressed = [UIImage imageNamed:@"Grid-Selected.png"];
        [GridButton setImage:GridButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:GridButton];
        GridSelected = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"GridSelVal"];
    }
}

-(IBAction)ShoesButton {
    if (ShoesSelected){
        Ltarget.hidden = YES;
        Rtarget.hidden = YES;
        UIImage *ShoesButtonPressed = [UIImage imageNamed:@"Shoe-Targets-Up.png"];
        [ShoesButton setImage:ShoesButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:ShoesButton];
        ShoesSelected = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"ShoesSelVal"];
    }
    else{
        Ltarget.hidden = NO;
        Rtarget.hidden = NO;
        UIImage *ShoesButtonPressed = [UIImage imageNamed:@"ShoeTargets-Selected.png"];
        [ShoesButton setImage:ShoesButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:ShoesButton];
        ShoesSelected = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ShoesSelVal"];
    }
}

-(IBAction)ZeroFSRButton {
    if (ZeroFSRSelected){
        UIImage *ZeroFSRButtonPressed = [UIImage imageNamed:@"zero_up.png"];
        //        UIImage *ZeroFSRButtonPressed = [UIImage imageNamed:@"Crosshair_Up.png"];
        [ZeroFSRButton setImage:ZeroFSRButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:ZeroFSRButton];
        ZeroFSRSelected = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"ZeroFSRSelVal"];
    }
    else{
        UIImage *ZeroFSRButtonPressed = [UIImage imageNamed:@"zero_selected.png"];
        //        UIImage *ZeroFSRButtonPressed = [UIImage imageNamed:@"Crosshair_Selected.png"];
        [ZeroFSRButton setImage:ZeroFSRButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:ZeroFSRButton];
        ZeroFSR1 = true;
        ZeroFSR2 = true;
        ZeroFSRSelected = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ZeroFSRSelVal"];
    }
}

-(IBAction)RawButton {
    if (RawSelected){
        UIImage *RawButtonPressed = [UIImage imageNamed:@"Numbers_Up.png"];
        [RawButton setImage:RawButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:RawButton];
        RawSelected = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"RawSelVal"];
    }
    else{
        UIImage *RawButtonPressed = [UIImage imageNamed:@"Numbers_Selected.png"];
        [RawButton setImage:RawButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:RawButton];
        RawSelected = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RawSelVal"];
    }
}

-(IBAction)More1Button {
    setupView.hidden = false;
    TargetSelectedButton.hidden = true;
    ShoesButton.hidden = true;
    GridButton.hidden = true;
    AccButton.hidden = true;
    AlarmButton.hidden = true;
    LimitsButton.hidden = true;
    DisplayIndicatorButton.hidden = true;
    ChangeColor1Button.hidden = true;
    ChangeColor2Button.hidden = true;
    ChangeColor3Button.hidden = true;
    ChangeColor4Button.hidden = true;
    TSUIScanForPeripheralsButton.hidden = true;
    TSUIScanForPeripheralsButtonL.hidden = true;
    TSUIScanForPeripheralsButtonAcc.hidden = true;
    
    feetButton.hidden = YES;
    balanceButton.hidden = YES;

    
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
        RawButton.hidden = true;
        ZeroFSRButton.hidden = true;
        
        //       [self.view addSubview:_chart];
        //       _chart.datasource = self;
        //       _chart.hidden = false;
        
    }
    else { //iPhone
        RawButton.hidden = false;
        ZeroFSRButton.hidden = false;
    }
}

-(IBAction)More2Button {
    setupView.hidden = true;
    TargetSelectedButton.hidden = false;
    ShoesButton.hidden = false;
    GridButton.hidden = false;
//    AccButton.hidden = false;
    AlarmButton.hidden = false;
    LimitsButton.hidden = false;
    DisplayIndicatorButton.hidden = false;
    if (BalanceMode){
        ChangeColor1Button.hidden = false;
        ChangeColor2Button.hidden = false;
        ChangeColor3Button.hidden = false;
        ChangeColor4Button.hidden = false;
    }
    TSUIScanForPeripheralsButton.hidden = false;
    TSUIScanForPeripheralsButtonL.hidden = false;
    
    feetButton.hidden = NO;
    balanceButton.hidden = NO;
    
    if (AccSelected){
        TSUIScanForPeripheralsButtonAcc.hidden = false;
    }
    else {
        TSUIScanForPeripheralsButtonAcc.hidden = true;
    }
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
        RawButton.hidden = false;
        ZeroFSRButton.hidden = false;
    }
    else { //iPhone
        RawButton.hidden = true;
        ZeroFSRButton.hidden = true;
    }
}

-(IBAction)AccButton {
    if (AccSelected){
        UIImage *AccButtonPressed = [UIImage imageNamed:@"Motion_Up.png"];
        [AccButton setImage:AccButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:AccButton];
        accelerometer.hidden = YES;
        accelerometerBatt.hidden = YES;
        label12.hidden = YES;
        label13.hidden = YES;
        label14.hidden = YES;
        label27.hidden = YES;
        label28.hidden = YES;
        label29.hidden = YES;
        puttT.hidden = YES;
        puttS.hidden = YES;
        arrow2.hidden = YES;
        arrow4.hidden = YES;
        acc45deg.hidden = YES;
        acc45degs.hidden = YES;
        acc45degs.enabled = NO;
        accvertical.hidden = YES;
        accverticals.hidden = YES;
        accverticals.enabled = NO;
        acchorizontal.hidden = YES;
        acchorizontals.hidden = YES;
        acchorizontals.enabled = NO;
        TSUIBatteryBarAcc.hidden = YES;
        TSUIScanForPeripheralsButtonAcc.hidden = YES;
        TSUIScanForPeripheralsButtonAcc.enabled = NO;
        //        TSUISpinnerAcc.hidden = YES;
        labelAP.hidden = YES;
        labelLateral.hidden = YES;
        labelVertical.hidden = YES;
        
        if(LimitsIndicator) {
            label4.hidden = NO;     //un-hide edges
            label22.hidden = NO;
            label8.hidden = NO;
            label21.hidden = NO;
            label32.hidden = NO;
            label37.hidden = NO;
            label30.hidden = NO;
            label35.hidden = NO;
            label34.hidden = NO;
            label39.hidden = NO;
            label25.hidden = NO;
            label26.hidden = NO;
            RLs2.hidden = NO;
            RLs.hidden = NO;
            lhorizontals.hidden = NO;
            lhorizontals2.hidden = NO;
            horizontals2.hidden = NO;
            horizontals.hidden = NO;
        }
        
        AccSelected = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"AccSelVal"];
    }
    else{
        UIImage *AccButtonPressed = [UIImage imageNamed:@"Motion_Selected.png"];
        [AccButton setImage:AccButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:AccButton];
        accelerometer.hidden = NO;
        accelerometerBatt.hidden = NO;
        //        label12.hidden = NO;
        //        label13.hidden = NO;
        //        label14.hidden = NO;
        //        label27.hidden = NO;
        //        label28.hidden = NO;
        //        label29.hidden = NO;
        if (dispMode == 0) {
            puttT.hidden = NO;
            puttS.hidden = NO;
            labelAP.hidden = YES;
            labelLateral.hidden = YES;
            labelVertical.hidden = YES;
        }
        else {
            puttT.hidden = YES;
            puttS.hidden = YES;
            labelAP.hidden = NO;
            labelLateral.hidden = NO;
            labelVertical.hidden = NO;
        }
        arrow2.hidden = NO;
        arrow4.hidden = NO;
        //        acc45deg.hidden = NO;
        //        acc45degs.hidden = NO;
        //        acc45degs.enabled = YES;
        //        accvertical.hidden = NO;
        //        accverticals.hidden = NO;
        //        accverticals.enabled = YES;
        //        acchorizontal.hidden = NO;
        //        acchorizontals.hidden = NO;
        //        acchorizontals.enabled = YES;
        TSUIBatteryBarAcc.hidden = NO;
        TSUIScanForPeripheralsButtonAcc.hidden = NO;
        TSUIScanForPeripheralsButtonAcc.enabled = YES;
        //        TSUISpinnerAcc.hidden = NO;
        AccSelected = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"AccSelVal"];
        if (!LimitsIndicator){
            acc45degs.hidden = YES;
            acc45degs.enabled = NO;
            accverticals.hidden = YES;
            accverticals.enabled = NO;
            acchorizontals.hidden = YES;
            acchorizontals.enabled = NO;
            label12.hidden = YES;
            label13.hidden = YES;
            label14.hidden = YES;
            label27.hidden = YES;
            label28.hidden = YES;
            label29.hidden = YES;
        }else {
            acc45degs.hidden = NO;
            acc45degs.enabled = YES;
            accverticals.hidden = NO;
            accverticals.enabled = YES;
            acchorizontals.hidden = NO;
            acchorizontals.enabled = YES;
            label12.hidden = NO;
            label13.hidden = NO;
            label14.hidden = NO;
            label27.hidden = NO;
            label28.hidden = NO;
            label29.hidden = NO;
            
            label4.hidden = YES;     //hide the edges
            label22.hidden = YES;
            label8.hidden = YES;
            label21.hidden = YES;
            label32.hidden = YES;
            label37.hidden = YES;
            label30.hidden = YES;
            label35.hidden = YES;
            label34.hidden = YES;
            label39.hidden = YES;
            label25.hidden = YES;
            label26.hidden = YES;
            RLs2.hidden = YES;
            RLs.hidden = YES;
            lhorizontals.hidden = YES;
            lhorizontals2.hidden = YES;
            horizontals2.hidden = YES;
            horizontals.hidden = YES;
            
        }
        if (!DisplayIndicator){
            acc45deg.hidden = YES;
            accvertical.hidden = YES;
            acchorizontal.hidden = YES;
        }else {
            acc45deg.hidden = NO;
            accvertical.hidden = NO;
            acchorizontal.hidden = NO;
        }
    }
}


-(IBAction)TargetSelectedButton {
    if (TargetSelected){
        target.hidden = YES;
        UIImage *TargetButtonPressed = [UIImage imageNamed:@"Target-Up.png"];
        [TargetSelectedButton setImage:TargetButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:TargetSelectedButton];
        TargetSelected = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TargetSelVal"];
    }else{
        target.hidden = NO;
        //        target.image = [UIImage imageNamed:@"target.png"];
        UIImage *TargetButtonPressed = [UIImage imageNamed:@"Target-Selected.png"];
        [TargetSelectedButton setImage:TargetButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:TargetSelectedButton];
        TargetSelected = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"TargetSelVal"];
    }
}

-(IBAction)DisplayIndicatorButton {
    if (DisplayIndicator){
        Lhind.hidden = YES;
        Rhind.hidden = YES;
        Lvind.hidden = YES;
        Rvind.hidden = YES;
        RLindicator.hidden = YES;
        label6.hidden = YES;
        label7.hidden = YES;
        label10.hidden = YES;
        label11.hidden = YES;
        label17.hidden = YES;
        label18.hidden = YES;
        label19.hidden = YES;
        label20.hidden = YES;
        label40.hidden = YES;
        label41.hidden = YES;
        label42.hidden = YES;
        label43.hidden = YES;
        label44.hidden = YES;
        label45.hidden = YES;
        label46.hidden = YES;
        label47.hidden = YES;
        label48.hidden = YES;
        label49.hidden = YES;
        label50.hidden = YES;
        label51.hidden = YES;
        DisplayIndicator = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"DisplayIndicatorSelVal"];
        UIImage *DisplayIndicatorButtonPressed = [UIImage imageNamed:@"Indicators-Up.png"];
        [DisplayIndicatorButton setImage:DisplayIndicatorButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:DisplayIndicatorButton];
        acc45deg.hidden = YES;
        accvertical.hidden = YES;
        acchorizontal.hidden = YES;
        
    }
    else{
        Lhind.hidden = NO;
        Rhind.hidden = NO;
        Lvind.hidden = NO;
        Rvind.hidden = NO;
        RLindicator.hidden = NO;
        label6.hidden = NO;
        label7.hidden = NO;
        label10.hidden = NO;
        label11.hidden = NO;
        label17.hidden = NO;
        label18.hidden = NO;
        label19.hidden = NO;
        label20.hidden = NO;
        label40.hidden = NO;
        label41.hidden = NO;
        label42.hidden = NO;
        label43.hidden = NO;
        label44.hidden = NO;
        label45.hidden = NO;
        label46.hidden = NO;
        label47.hidden = NO;
        label48.hidden = NO;
        label49.hidden = NO;
        label50.hidden = NO;
        label51.hidden = NO;
        DisplayIndicator = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"DisplayIndicatorSelVal"];
        UIImage *DisplayIndicatorButtonPressed = [UIImage imageNamed:@"Indicators-Selected.png"];
        [DisplayIndicatorButton setImage:DisplayIndicatorButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:DisplayIndicatorButton];
        if (AccSelected){
            acc45deg.hidden = NO;
            accvertical.hidden = NO;
            acchorizontal.hidden = NO;
        }
    }
}

-(IBAction)LimitsButton {
    if (LimitsIndicator){
        verticals.hidden = YES;
        horizontals.hidden = YES;
        lverticals.hidden = YES;
        lhorizontals.hidden = YES;
        RLs.hidden = YES;
        verticals2.hidden = YES;
        horizontals2.hidden = YES;
        lverticals2.hidden = YES;
        lhorizontals2.hidden = YES;
        RLs2.hidden = YES;
        label4.hidden = YES;
        label5.hidden = YES;
        label8.hidden = YES;
        label9.hidden = YES;
        label21.hidden = YES;
        label22.hidden = YES;
        label23.hidden = YES;
        label24.hidden = YES;
        label25.hidden = YES;
        label26.hidden = YES;
        label30.hidden = YES;
        label31.hidden = YES;
        label32.hidden = YES;
        label33.hidden = YES;
        label34.hidden = YES;
        label35.hidden = YES;
        label36.hidden = YES;
        label37.hidden = YES;
        label38.hidden = YES;
        label39.hidden = YES;
        LimitsIndicator = false;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"LimitsIndicatorSelVal"];
        UIImage *LimitsButtonPressed = [UIImage imageNamed:@"Contraints-Up.png"];
        [LimitsButton setImage:LimitsButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:LimitsButton];
        acc45degs.hidden = YES;
        acc45degs.enabled = NO;
        accverticals.hidden = YES;
        accverticals.enabled = NO;
        acchorizontals.hidden = YES;
        acchorizontals.enabled = NO;
        label12.hidden = YES;
        label13.hidden = YES;
        label14.hidden = YES;
        label27.hidden = YES;
        label28.hidden = YES;
        label29.hidden = YES;
    }
    else{
        verticals.hidden = NO;
        horizontals.hidden = NO;
        lverticals.hidden = NO;
        lhorizontals.hidden = NO;
        RLs.hidden = NO;
        verticals2.hidden = NO;
        horizontals2.hidden = NO;
        lverticals2.hidden = NO;
        lhorizontals2.hidden = NO;
        RLs2.hidden = NO;
        label4.hidden = NO;
        label5.hidden = NO;
        label8.hidden = NO;
        label9.hidden = NO;
        label21.hidden = NO;
        label22.hidden = NO;
        label23.hidden = NO;
        label24.hidden = NO;
        label25.hidden = NO;
        label26.hidden = NO;
        label30.hidden = NO;
        label31.hidden = NO;
        label32.hidden = NO;
        label33.hidden = NO;
        label34.hidden = NO;
        label35.hidden = NO;
        label36.hidden = NO;
        label37.hidden = NO;
        label38.hidden = NO;
        label39.hidden = NO;
        LimitsIndicator = true;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"LimitsIndicatorSelVal"];
        UIImage *LimitsButtonPressed = [UIImage imageNamed:@"Contraints-Selected.png"];
        [LimitsButton setImage:LimitsButtonPressed forState:UIControlStateNormal];
        [self.view addSubview:LimitsButton];
        if (AccSelected){
            acc45degs.hidden = NO;
            acc45degs.enabled = YES;
            accverticals.hidden = NO;
            accverticals.enabled = YES;
            acchorizontals.hidden = NO;
            acchorizontals.enabled = YES;
            label12.hidden = NO;
            label13.hidden = NO;
            label14.hidden = NO;
            label27.hidden = NO;
            label28.hidden = NO;
            label29.hidden = NO;
            
            label4.hidden = YES;     //hide the edges
            label22.hidden = YES;
            label8.hidden = YES;
            label21.hidden = YES;
            label32.hidden = YES;
            label37.hidden = YES;
            label30.hidden = YES;
            label35.hidden = YES;
            label34.hidden = YES;
            label39.hidden = YES;
            label25.hidden = YES;
            label26.hidden = YES;
            RLs2.hidden = YES;
            RLs.hidden = YES;
            lhorizontals.hidden = YES;
            lhorizontals2.hidden = YES;
            horizontals2.hidden = YES;
            horizontals.hidden = YES;
            
        }
    }
}

-(IBAction)ChangeColor1Button {
    ChangeColor1 = 1;
    ChangeColor2 = 0;
    ChangeColor3 = 0;
    ChangeColor4 = 0;
    UIImage *ChangeColor1ButtonPressed = [UIImage imageNamed:@"RedBtn_Selected.png"];
    [ChangeColor1Button setImage:ChangeColor1ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor1Button];
    UIImage *ChangeColor2ButtonPressed = [UIImage imageNamed:@"OrangeBtn_Up.png"];
    [ChangeColor2Button setImage:ChangeColor2ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor2Button];
    UIImage *ChangeColor3ButtonPressed = [UIImage imageNamed:@"YellowBtn_Up.png"];
    [ChangeColor3Button setImage:ChangeColor3ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor3Button];
    UIImage *ChangeColor4ButtonPressed = [UIImage imageNamed:@"GreenBtn_Up.png"];
    [ChangeColor4Button setImage:ChangeColor4ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor4Button];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ChgColorSelVal"];
}

-(IBAction)ChangeColor2Button {
    ChangeColor1 = 0;
    ChangeColor2 = 1;
    ChangeColor3 = 0;
    ChangeColor4 = 0;
    UIImage *ChangeColor1ButtonPressed = [UIImage imageNamed:@"RedBtn_Up.png"];
    [ChangeColor1Button setImage:ChangeColor1ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor1Button];
    UIImage *ChangeColor2ButtonPressed = [UIImage imageNamed:@"OrangeBtn_Selected.png"];
    [ChangeColor2Button setImage:ChangeColor2ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor2Button];
    UIImage *ChangeColor3ButtonPressed = [UIImage imageNamed:@"YellowBtn_Up.png"];
    [ChangeColor3Button setImage:ChangeColor3ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor3Button];
    UIImage *ChangeColor4ButtonPressed = [UIImage imageNamed:@"GreenBtn_Up.png"];
    [ChangeColor4Button setImage:ChangeColor4ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor4Button];
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"ChgColorSelVal"];
}

-(IBAction)ChangeColor3Button {
    ChangeColor1 = 0;
    ChangeColor2 = 0;
    ChangeColor3 = 1;
    ChangeColor4 = 0;
    UIImage *ChangeColor1ButtonPressed = [UIImage imageNamed:@"RedBtn_Up.png"];
    [ChangeColor1Button setImage:ChangeColor1ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor1Button];
    UIImage *ChangeColor2ButtonPressed = [UIImage imageNamed:@"OrangeBtn_Up.png"];
    [ChangeColor2Button setImage:ChangeColor2ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor2Button];
    UIImage *ChangeColor3ButtonPressed = [UIImage imageNamed:@"YellowBtn_Selected.png"];
    [ChangeColor3Button setImage:ChangeColor3ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor3Button];
    UIImage *ChangeColor4ButtonPressed = [UIImage imageNamed:@"GreenBtn_Up.png"];
    [ChangeColor4Button setImage:ChangeColor4ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor4Button];
    [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"ChgColorSelVal"];
}

-(IBAction)ChangeColor4Button {
    ChangeColor1 = 0;
    ChangeColor2 = 0;
    ChangeColor3 = 0;
    ChangeColor4 = 1;
    UIImage *ChangeColor1ButtonPressed = [UIImage imageNamed:@"RedBtn_Up.png"];
    [ChangeColor1Button setImage:ChangeColor1ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor1Button];
    UIImage *ChangeColor2ButtonPressed = [UIImage imageNamed:@"OrangeBtn_Up.png"];
    [ChangeColor2Button setImage:ChangeColor2ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor2Button];
    UIImage *ChangeColor3ButtonPressed = [UIImage imageNamed:@"YellowBtn_Up.png"];
    [ChangeColor3Button setImage:ChangeColor3ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor3Button];
    UIImage *ChangeColor4ButtonPressed = [UIImage imageNamed:@"GreenBtn_Selected.png"];
    [ChangeColor4Button setImage:ChangeColor4ButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:ChangeColor4Button];
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"ChgColorSelVal"];
}

-(IBAction)GraphOnlyButton {
    GraphOnly = true;
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    if (sender == self.verticals){
        label5.text = [NSString stringWithFormat:@"%.f", [sender value]];
        vSliderVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[verticals value] forKey:@"sliderValuev"];
    }
    
    if (sender == self.horizontals){
        label4.text = [NSString stringWithFormat:@"%.f", [sender value]];
        hSliderVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[horizontals value] forKey:@"sliderValueh"];
    }
    
    if (sender == self.lverticals){
        label9.text = [NSString stringWithFormat:@"%.f", [sender value]];
        lvSliderVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[lverticals value] forKey:@"lsliderValuev"];
    }
    
    if (sender == self.lhorizontals){
        label8.text = [NSString stringWithFormat:@"%.f", [sender value]];
        lhSliderVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[lhorizontals value] forKey:@"lsliderValueh"];
    }
    
    if (sender == self.verticals2){
        label31.text = [NSString stringWithFormat:@"%.f", [sender value]];
        vSliderVal2 = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[verticals2 value] forKey:@"sliderValuev2"];
    }
    
    if (sender == self.horizontals2){
        label30.text = [NSString stringWithFormat:@"%.f", [sender value]];
        hSliderVal2 = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[horizontals2 value] forKey:@"sliderValueh2"];
    }
    
    if (sender == self.lverticals2){
        label33.text = [NSString stringWithFormat:@"%.f", [sender value]];
        lvSliderVal2 = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[lverticals2 value] forKey:@"lsliderValuev2"];
    }
    
    if (sender == self.lhorizontals2){
        label32.text = [NSString stringWithFormat:@"%.f", [sender value]];
        lhSliderVal2 = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[lhorizontals2 value] forKey:@"lsliderValueh2"];
    }
    
    if (sender == self.acchorizontals){
        label13.text = [NSString stringWithFormat:@"%.f", [sender value]];
        acchSliderVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[acchorizontals value] forKey:@"accsliderValueh"];
    }
    
    if (sender == self.accverticals){
        label12.text = [NSString stringWithFormat:@"%.f", [sender value]];
        accvSliderVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[accverticals value] forKey:@"accsliderValuev"];
    }
    
    if (sender == self.acc45degs){
        label14.text = [NSString stringWithFormat:@"%.f", [sender value]];
        acc45degSliderVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[acc45degs value] forKey:@"accsliderValue45deg"];
    }
    
    if (sender == self.RLs){
        label25.text = [NSString stringWithFormat:@"%.f", [sender value]];
        RLsVal = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[RLs value] forKey:@"RLsliderValue"];
    }
    
    if (sender == self.RLs2){
        label34.text = [NSString stringWithFormat:@"%.f", [sender value]];
        RLsVal2 = [sender value];
        [[NSUserDefaults standardUserDefaults] setFloat:[RLs2 value] forKey:@"RLsliderValue2"];
    }
    
	if (sender == self.frequencySlider){
        frequency = [sender value];
        frequencyLabel.text = [NSString stringWithFormat:@"%4.1f Hz", frequency];
        [[NSUserDefaults standardUserDefaults] setFloat:[frequencySlider value] forKey:@"sliderValuefreq"];
    }
    
	if (sender == self.volumeSlider){
        volume = [sender value];
        volumeLabel.text = [NSString stringWithFormat:@"%4.f", volume*10];
        [[NSUserDefaults standardUserDefaults] setFloat:[volumeSlider value] forKey:@"sliderValuevol"];
    }
    
	if (sender == self.patHeightSlider){
        patHeight = [sender value];
        feet = (int)patHeight/12;
        inches = (int)patHeight-(feet*12);
        feetLabel.text = [NSString stringWithFormat:@"%4.f", feet];
        inchesLabel.text = [NSString stringWithFormat:@"%4.f", inches];
        [[NSUserDefaults standardUserDefaults] setFloat:[patHeightSlider value] forKey:@"sliderValuepatHeight"];
    }
    
	if (sender == self.patStanceSlider){
        patStance = [sender value];
        patStanceLabel.text = [NSString stringWithFormat:@"%4.f", patStance];
        [[NSUserDefaults standardUserDefaults] setFloat:[patStanceSlider value] forKey:@"sliderValuepatStance"];
    }
}

- (void)createToneUnit
{
	// Configure the search parameters to find the default playback output unit
	// (called the kAudioUnitSubType_RemoteIO on iOS but
	// kAudioUnitSubType_DefaultOutput on Mac OS X)
	AudioComponentDescription defaultOutputDescription;
	defaultOutputDescription.componentType = kAudioUnitType_Output;
	defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
	defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
	defaultOutputDescription.componentFlags = 0;
	defaultOutputDescription.componentFlagsMask = 0;
	
	// Get the default playback output unit
	AudioComponent defaultOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
	NSAssert(defaultOutput, @"Can't find default output");
	
	// Create a new unit based on this that we'll use for output
	OSErr err = AudioComponentInstanceNew(defaultOutput, &toneUnit);
	NSAssert1(toneUnit, @"Error creating unit: %hd", err);
	
	// Set our tone rendering function on the unit
	AURenderCallbackStruct input;
	input.inputProc = RenderTone;
	input.inputProcRefCon = (__bridge void *)(self);
	err = AudioUnitSetProperty(toneUnit,
                               kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input,
                               0,
                               &input,
                               sizeof(input));
	NSAssert1(err == noErr, @"Error setting callback: %hd", err);
	
	// Set the format to 32 bit, single channel, floating point, linear PCM
	const int four_bytes_per_float = 4;
	const int eight_bits_per_byte = 8;
	AudioStreamBasicDescription streamFormat;
	streamFormat.mSampleRate = sampleRate;
	streamFormat.mFormatID = kAudioFormatLinearPCM;
	streamFormat.mFormatFlags =
    kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	streamFormat.mBytesPerPacket = four_bytes_per_float;
	streamFormat.mFramesPerPacket = 1;
	streamFormat.mBytesPerFrame = four_bytes_per_float;
	streamFormat.mChannelsPerFrame = 1;
	streamFormat.mBitsPerChannel = four_bytes_per_float * eight_bits_per_byte;
	err = AudioUnitSetProperty (toneUnit,
                                kAudioUnitProperty_StreamFormat,
                                kAudioUnitScope_Input,
                                0,
                                &streamFormat,
                                sizeof(AudioStreamBasicDescription));
	NSAssert1(err == noErr, @"Error setting stream format: %hd", err);
}


- (IBAction)togglePlay:(UIButton *)selectedButton
{
	if (toneUnit)
	{
		AudioOutputUnitStop(toneUnit);
		AudioUnitUninitialize(toneUnit);
		AudioComponentInstanceDispose(toneUnit);
		toneUnit = nil;
		
		[selectedButton setTitle:NSLocalizedString(@"Play", nil) forState:0];
	}
	else
	{
		[self createToneUnit];
		
		// Stop changing parameters on the unit
		OSErr err = AudioUnitInitialize(toneUnit);
		NSAssert1(err == noErr, @"Error initializing unit: %hd", err);
		
		// Start playback
		err = AudioOutputUnitStart(toneUnit);
		NSAssert1(err == noErr, @"Error starting unit: %hd", err);
		
		[selectedButton setTitle:NSLocalizedString(@"Stop", nil) forState:0];
	}
}


- (void)stop
{
	if (toneUnit && !AlarmSelected)
	{
		[self togglePlay:playButton];
	}
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle






- (void)viewDidLoad
{
    
    /*
     
     */
    
    self.testValues = [[NSMutableArray alloc] init];
    
    NSString *tmpFileName = @"ztmp.csv";
    NSString *tmpFileNamePDF = @"ztmp.pdf";
    
    NSArray *docPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [docPath objectAtIndex:0];
    
    NSString *fileTest = [documentsDirectoryPath stringByAppendingPathComponent:tmpFileName];
    NSString *fileTestPDF = [documentsDirectoryPath stringByAppendingPathComponent:tmpFileNamePDF];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:fileTest error:NULL];
    [fileManager removeItemAtPath:fileTestPDF error:NULL];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;

    if (iOSDeviceScreenSize.height == 480) //480
        device4s = true;
        
    
    //    self.view.backgroundColor = [UIColor whiteColor];
    //    _chart1.chartBackgroundColor = [UIColor whiteColor];
    //    _chart1.background.setBackgroundColor = [UIColor blueColor];
    //    _chart1.legend.style.areaColor = [UIColor redColor];
    
    //    CGFloat margin = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? 10.0 : 50.0;
    //    _chart = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.view.bounds, margin, margin)];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
        //        _chart1 = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.view.bounds, 50, 50)];
        //        _chart2 = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.view.bounds, 50, 50)];
        //        _chart3 = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.view.bounds, 100, 300)];
        _chart1 = [[ShinobiChart alloc] initWithFrame:CGRectMake(50, 120, 668, 560)];
        //        _chart2 = [[ShinobiChart alloc] initWithFrame:CGRectMake(50, 376, 668, 280)];
        //        _chart3 = [[ShinobiChart alloc] initWithFrame:CGRectMake(50, 672, 668, 280)];
        
    }
    else { //iPhone
        //        _chart1 = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.view.bounds, 5, 175)];
        //        _chart2 = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.view.bounds, 5, 175)];
        //        _chart3 = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.view.bounds, 5, 175)];
        _chart1 = [[ShinobiChart alloc] initWithFrame:CGRectMake(20, 82, 280, 285)];
        //        _chart2 = [[ShinobiChart alloc] initWithFrame:CGRectMake(20, 204, 280, 150)];
        //        _chart3 = [[ShinobiChart alloc] initWithFrame:CGRectMake(20, 356, 280, 150)];
        
    }
    
    
    //Shinobi's suggestion to define theme at chart creation...
    //    self.chart = [[ShinobiChart alloc] initWithFrame:chartFrame withTheme:[SChartLightTheme new]];
    
    
    [self->_chart1 applyTheme:[SChartLightTheme new]]; //iOS6 theme
    _chart1.licenseKey = kChartLicenseKey;
    
    _chart1.title = @"Results";
    
    _chart1.titlePosition = SChartTitlePositionCenter;
    
    //    _chart2.title = @"2 Results";
    //    _chart3.title = @"3 Results";
    
    
    _chart1.autoresizingMask =  ~UIViewAutoresizingNone;
    //    _chart2.autoresizingMask =  ~UIViewAutoresizingNone;
    //    _chart3.autoresizingMask =  ~UIViewAutoresizingNone;
    
    // add a pair of axes
    SChartNumberAxis *xAxis1 = [[SChartNumberAxis alloc] init];
    xAxis1.title = @"Seconds";
    //    xAxis1.style.lineColor = [UIColor redColor];
    _chart1.xAxis = xAxis1;
    
    _chart1.xAxis.style.titleStyle.position = SChartTitlePositionCenter;
    
    //    _chart1.autoCalculateAxisRanges = NO;
    
    
    /*
     SChartNumberAxis *xAxis2 = [[SChartNumberAxis alloc] init];
     xAxis2.title = @"X Value";
     _chart2.xAxis = xAxis2;
     SChartNumberAxis *xAxis3 = [[SChartNumberAxis alloc] init];
     xAxis3.title = @"X Value";
     _chart3.xAxis = xAxis3;
     */
    
    //    SChartRange *yRange = _chart1.yAxis.visibleRange;
    //    NSNumber *yMin = yRange.minimum;
    //    NSNumber *yMax = yRange.maximum;
    //    SChartNumberRange* numberRange = [[SChartNumberRange alloc] initWithMinimum:yMin andMaximum:yMax];
    //    SChartNumberAxis* yAxis1 = [[SChartNumberAxis alloc] initWithRange:numberRange];
    
    SChartNumberAxis *yAxis1 = [[SChartNumberAxis alloc] init];
    //    yAxis1.title = @"Y Value";
    //    yAxis1.rangePaddingLow = @1;
    yAxis1.rangePaddingHigh = @5; //20.0
    _chart1.yAxis = yAxis1;
    
    /*
     SChartNumberAxis *yAxis2 = [[SChartNumberAxis alloc] init];
     yAxis2.title = @"Y Value";
     yAxis2.rangePaddingLow = @(0.1);
     yAxis2.rangePaddingHigh = @(0.1);
     _chart2.yAxis = yAxis2;
     SChartNumberAxis *yAxis3 = [[SChartNumberAxis alloc] init];
     yAxis3.title = @"Y Value";
     yAxis3.rangePaddingLow = @(0.1);
     yAxis3.rangePaddingHigh = @(0.1);
     _chart3.yAxis = yAxis3;
     */
    
    // enable gestures
    yAxis1.enableGesturePanning = YES;
    yAxis1.enableGestureZooming = YES;
    xAxis1.enableGesturePanning = YES;
    xAxis1.enableGestureZooming = YES;
    /*
     yAxis2.enableGesturePanning = YES;
     yAxis2.enableGestureZooming = YES;
     xAxis2.enableGesturePanning = YES;
     xAxis2.enableGestureZooming = YES;
     yAxis3.enableGesturePanning = YES;
     yAxis3.enableGestureZooming = YES;
     xAxis3.enableGesturePanning = YES;
     xAxis3.enableGestureZooming = YES;
     */
    _chart1.xAxis.enableMomentumPanning = YES;
    _chart1.xAxis.enableMomentumZooming = YES;
    _chart1.gestureDoubleTapResetsZoom = YES;
    xAxis1.style.minorTickStyle.showTicks = YES;
    yAxis1.style.minorTickStyle.showTicks = YES;
    /*
     _chart2.xAxis.enableMomentumPanning = YES;
     _chart2.xAxis.enableMomentumZooming = YES;
     _chart2.gestureDoubleTapResetsZoom = YES;
     _chart3.xAxis.enableMomentumPanning = YES;
     _chart3.xAxis.enableMomentumZooming = YES;
     _chart3.gestureDoubleTapResetsZoom = YES;
     */
    
    //************************************************************************************
    //    _chart1.legend.hidden = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
    //************************************************************************************
    
    
    //    target.image = [UIImage imageNamed:@"target.png"];
    
    //    UIImageView *newTarget = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"target.png"]];
    //    self.target = newTarget;
    //    self.target.userInteractionEnabled = YES;
    //    self.target.frame = CGRectMake(0, 0, 122, 122);
    //    [self.view addSubview:self.target];
    //    [newTarget release];
    
    //AlarmLine = 0;
    grid.hidden = YES;
    alarm.hidden = YES;
//    drawing.hidden = NO;
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    t = AppDelegate.rightInsole;   // Init TruSensors class.
    t.delegate = self;                  // Set TruSensors delegate class to point at methods implemented in this class.
    
    if (t.activePeripheral)
    {
        t.fsr1 = 0;
        t.fsr2 = 0;
        t.fsr3 = 0;
        t.fsr4 = 0;
        // Start battery indicator timer, calls batteryIndicatorTimer method every 2 seconds
        battTimer = [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(batteryIndicatorTimer:) userInfo:nil repeats:YES];
        
        [t enableFSR:[t activePeripheral]];             // Enable fsr (if found)
        [t enableButtons:[t activePeripheral]];         // Enable button service (if found)
        [t enableTXPower:[t activePeripheral]];         // Enable TX power service (if found)
        [TSUISpinner stopAnimating];
        UIImage *rightConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
        [TSUIScanForPeripheralsButton setImage:rightConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButton];
    }
    
    t2 = AppDelegate.leftInsole;   // Init TruSensors class.
    t2.delegate = self;                  // Set TruSensors delegate class to point at methods implemented in this class.
    
    if (t2.activePeripheral)
    {
        t2.fsr1 = 0;
        t2.fsr2 = 0;
        t2.fsr3 = 0;
        t2.fsr4 = 0;
        
        // Start battery indicator timer, calls batteryIndicatorTimer method every 2 seconds
        battTimer = [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(batteryIndicatorTimerL:) userInfo:nil repeats:YES];
        
        [t2 enableFSR:[t2 activePeripheral]];             // Enable fsr (if found)
        [t2 enableButtons:[t2 activePeripheral]];         // Enable button service (if found)
        [t2 enableTXPower:[t2 activePeripheral]];         // Enable TX power service (if found)
        [TSUISpinner stopAnimating];
        UIImage *rightConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
        [TSUIScanForPeripheralsButtonL setImage:rightConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButtonL];
    }
    
    t3 = [[TruSensors alloc] init];   // Init TruSensors class.
    [t3 controlSetup:3];                 // Do initial setup of TruSensors class.
    t3.delegate = self;                  // Set TruSensors delegate class to point at methods implemented in this class.
    
    target.image = [UIImage imageNamed:@"target.png"];
    
    UIImage *minImage = [UIImage imageNamed:@"grey_track.png"];
    UIImage *maxImage = [UIImage imageNamed:@"grey_track.png"];
    UIImage *thumbImageBT= [UIImage imageNamed:@"ConstraintArrow_Blue_Vertical_Top.png"];
    UIImage *thumbImageBB= [UIImage imageNamed:@"ConstraintArrow_Blue_Vertical_Bottom.png"];
    UIImage *thumbImageBB2= [UIImage imageNamed:@"ConstraintArrow_Blue_Vertical_Bottom2.png"];
    UIImage *thumbImageBL= [UIImage imageNamed:@"ConstraintArrow_Blue_Horizontal_Left.png"];
    UIImage *thumbImageBR= [UIImage imageNamed:@"ConstraintArrow_Blue_Horizontal_Right.png"];
    UIImage *thumbImageRT= [UIImage imageNamed:@"ConstraintArrow_Vertical_Top.png"];
    UIImage *thumbImageRT2= [UIImage imageNamed:@"ConstraintArrow_Vertical_Top2.png"];
    UIImage *thumbImageRB= [UIImage imageNamed:@"ConstraintArrow_Vertical_Bottom.png"];
    UIImage *thumbImageRB2= [UIImage imageNamed:@"ConstraintArrow_Vertical_Bottom2.png"];
    UIImage *thumbImageRL= [UIImage imageNamed:@"ConstraintArrow_Horizontal_Left.png"];
    UIImage *thumbImageRR= [UIImage imageNamed:@"ConstraintArrow_Horizontal_Right.png"];
    UIImage *thumbImageRL2= [UIImage imageNamed:@"ConstraintArrow_Horizontal_Left2.png"];
    UIImage *thumbImageRR2= [UIImage imageNamed:@"ConstraintArrow_Horizontal_Right2.png"];
    minImage=[minImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    maxImage=[maxImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    [RLindicator setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [RLindicator setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [Rvind setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [Rvind setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [Lvind setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [Lvind setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [Rhind setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [Rhind setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [Lhind setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [Lhind setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [RLindicator setThumbImage:thumbImageBT forState:UIControlStateNormal];
    [Rvind setThumbImage:thumbImageBL forState:UIControlStateNormal];
    [Lvind setThumbImage:thumbImageBR forState:UIControlStateNormal];
    [Rhind setThumbImage:thumbImageBB forState:UIControlStateNormal];
    [Lhind setThumbImage:thumbImageBB2 forState:UIControlStateNormal];
    
    
    [RLs setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [RLs setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [verticals setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [verticals setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [horizontals setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [horizontals setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [lverticals setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [lverticals setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [lhorizontals setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [lhorizontals setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [RLs setThumbImage:thumbImageRB2 forState:UIControlStateNormal];
    [verticals setThumbImage:thumbImageRR2 forState:UIControlStateNormal];
    [lverticals setThumbImage:thumbImageRL2 forState:UIControlStateNormal];
    [horizontals setThumbImage:thumbImageRT forState:UIControlStateNormal];
    [lhorizontals setThumbImage:thumbImageRT2 forState:UIControlStateNormal];
    
    [RLs2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [RLs2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [verticals2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [verticals2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [horizontals2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [horizontals2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [lverticals2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [lverticals2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [lhorizontals2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [lhorizontals2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [RLs2 setThumbImage:thumbImageRB forState:UIControlStateNormal];
    [verticals2 setThumbImage:thumbImageRR forState:UIControlStateNormal];
    [lverticals2 setThumbImage:thumbImageRL forState:UIControlStateNormal];
    [horizontals2 setThumbImage:thumbImageRT2 forState:UIControlStateNormal];
    [lhorizontals2 setThumbImage:thumbImageRT forState:UIControlStateNormal];
    
    
    //    UIImage *ChangeColor1ButtonPressed = [UIImage imageNamed:@"RedBtn_Selected.png"];
    //    [ChangeColor1Button setImage:ChangeColor1ButtonPressed forState:UIControlStateNormal];
    //    [self.view addSubview:ChangeColor1Button];
    
    
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * 1.5);
    //    vertical.transform = trans;
    
    //    lvertical.transform = trans;
    //    accvertical.transform = trans;
    Rvind.transform = trans;
    Lvind.transform = trans;
    
    //    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * 1.5);
    //    verticals.transform = trans;
    verticals2.transform = trans;
    accverticals.transform = trans;
    
    //    lverticals.transform = trans;
    lverticals2.transform = trans;
    
    CGAffineTransform trans3 = CGAffineTransformMakeRotation(M_PI * 0.5);
    verticals.transform = trans3;
    lverticals.transform = trans3;
    
    
    CGAffineTransform trans4 = CGAffineTransformMakeScale(1.0, 4.0);
    
    
    
    //    [accvertical setTransform:CGAffineTransformMakeScale(1.0, 4.0)];
    //    [acc45deg setTransform:CGAffineTransformMakeScale(1.0, 4.0)];
    //    [acchorizontal setTransform:CGAffineTransformMakeScale(1.0, 4.0)];
    //    [accvertical setFrame:CGRectMake(50, 425, 218, 9)];
    //    [acc45deg setFrame:CGRectMake(50, 425, 218, 9)];
    //    [acchorizontal setFrame:CGRectMake(50, 425, 218, 9)];
    
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        iOS_offset = 20; // if iOS6
        
        [TSUIBatteryBar setFrame:CGRectMake(725, 49, 24, 9)];
        [TSUIBatteryBar setTransform:CGAffineTransformMakeScale(1.0, 1.2)];
        [TSUIBatteryBarL setFrame:CGRectMake(19, 49, 24, 9)];
        [TSUIBatteryBarL setTransform:CGAffineTransformMakeScale(1.0, 1.2)];
        [TSUIBatteryBarAcc setFrame:CGRectMake(432, 118, 24, 9)];
        [TSUIBatteryBarAcc setTransform:CGAffineTransformMakeScale(1.0, 1.2)];
        
        accvertical.transform = trans;
        acc45deg.transform = trans;
        accvertical.center = CGPointMake(166, 327);
        acc45deg.center = CGPointMake(605, 327);
        acchorizontal.center = CGPointMake(166, 327);
        
    } else {
        iOS_offset = 0; // if iOS7
        
        [TSUIBatteryBar setTransform:CGAffineTransformMakeScale(1.0, 5.0)];
        [TSUIBatteryBarL setTransform:CGAffineTransformMakeScale(1.0, 5.0)];
        [TSUIBatteryBarAcc setTransform:CGAffineTransformMakeScale(1.0, 5.0)];
        
        acchorizontal.transform = trans4;
        acc45deg.transform = CGAffineTransformConcat(trans4, trans);
        accvertical.transform = CGAffineTransformConcat(trans4, trans);
        
    }
    
   
    
    NSInteger initialSetup = [[NSUserDefaults standardUserDefaults] integerForKey:@"initialSetupValue"];
    if (initialSetup == 0) {

        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"sliderValuev"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"sliderValueh"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"lsliderValuev"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"lsliderValueh"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"sliderValuev2"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"sliderValueh2"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"lsliderValuev2"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"lsliderValueh2"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:50 forKey:@"accsliderValuev"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:50 forKey:@"accsliderValueh"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:50 forKey:@"accsliderValue45deg"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"RLsliderValue"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:100 forKey:@"RLsliderValue2"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:440 forKey:@"sliderValuefreq"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:0.2 forKey:@"sliderValuevol"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:72 forKey:@"sliderValuepatHeight"];
        
        [[NSUserDefaults standardUserDefaults] setFloat:72 forKey:@"sliderValuepatStance"];
        
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"initialSetupValue"];

    }
    
    
    [verticals setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValuev"]];
    label5.text = [NSString stringWithFormat:@"%.f", [verticals value]];
    vSliderVal = [verticals value];
    
    [horizontals setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValueh"]];
    label4.text = [NSString stringWithFormat:@"%.f", [horizontals value]];
    hSliderVal = [horizontals value];
    
    [lverticals setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"lsliderValuev"]];
    label9.text = [NSString stringWithFormat:@"%.f", [lverticals value]];
    lvSliderVal = [lverticals value];
    
    [lhorizontals setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"lsliderValueh"]];
    label8.text = [NSString stringWithFormat:@"%.f", [lhorizontals value]];
    lhSliderVal = [lhorizontals value];
    
    [verticals2 setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValuev2"]];
    label31.text = [NSString stringWithFormat:@"%.f", [verticals2 value]];
    vSliderVal2 = [verticals2 value];
    
    [horizontals2 setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValueh2"]];
    label30.text = [NSString stringWithFormat:@"%.f", [horizontals2 value]];
    hSliderVal2 = [horizontals2 value];
    
    [lverticals2 setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"lsliderValuev2"]];
    label33.text = [NSString stringWithFormat:@"%.f", [lverticals2 value]];
    lvSliderVal2 = [lverticals2 value];
    
    [lhorizontals2 setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"lsliderValueh2"]];
    label32.text = [NSString stringWithFormat:@"%.f", [lhorizontals2 value]];
    lhSliderVal2 = [lhorizontals2 value];
    
    [accverticals setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"accsliderValuev"]];
    label12.text = [NSString stringWithFormat:@"%.f", [accverticals value]];
    accvSliderVal = [accverticals value];
    
    [acchorizontals setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"accsliderValueh"]];
    label13.text = [NSString stringWithFormat:@"%.f", [acchorizontals value]];
    acchSliderVal = [acchorizontals value];
    
    [acc45degs setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"accsliderValue45deg"]];
    label14.text = [NSString stringWithFormat:@"%.f", [acc45degs value]];
    acc45degSliderVal = [acc45degs value];
    
    [RLs setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"RLsliderValue"]];
    label25.text = [NSString stringWithFormat:@"%.f", [RLs value]];
    RLsVal = [RLs value];
    
    [RLs2 setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"RLsliderValue2"]];
    label34.text = [NSString stringWithFormat:@"%.f", [RLs2 value]];
    RLsVal2 = [RLs2 value];
    
    [frequencySlider setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValuefreq"]];
    frequencyLabel.text = [NSString stringWithFormat:@"%4.1f Hz", [frequencySlider value]];
    frequency = [frequencySlider value];
    
    [volumeSlider setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValuevol"]];
    //    volumeLabel.text = [NSString stringWithFormat:@"%4.f", [volumeSlider value]];
    volume = [volumeSlider value];
    volumeLabel.text = [NSString stringWithFormat:@"%4.f", volume*10];
    
    [patHeightSlider setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValuepatHeight"]];
    patHeight = [patHeightSlider value];
    feet = (int)patHeight/12;
    inches = (int)patHeight-(feet*12);
    feetLabel.text = [NSString stringWithFormat:@"%4.f", feet];
    inchesLabel.text = [NSString stringWithFormat:@"%4.f", inches];
    
    [patStanceSlider setValue:[[NSUserDefaults standardUserDefaults] floatForKey:@"sliderValuepatStance"]];
    patStance = [patStanceSlider value];
    patStanceLabel.text = [NSString stringWithFormat:@"%4.f", patStance];
    
    sampleRate = 44100;
    //    frequency = 440;
    //    volume = 25;
    
    dispMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"dispModeVal"];
    if (dispMode > 1)
        dispMode = 0;
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"AlarmSelVal"];
    if (tmpMode == 0){
        AlarmSelected = false;
    }
    else
    {
        AlarmSelected = true;
    }
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"GridSelVal"];
    if (tmpMode == 0){
        GridSelected = false;
    }
    else
    {
        GridSelected = true;
    }
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"ShoesSelVal"];
    if (tmpMode == 0){
        ShoesSelected = false;
    }
    else
    {
        ShoesSelected = true;
    }
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"ZeroFSRSelVal"];
    if (tmpMode == 0){
        ZeroFSRSelected = false;
    }
    else
    {
        ZeroFSRSelected = true;
    }
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"RawSelVal"];
    if (tmpMode == 0){
        RawSelected = false;
    }
    else
    {
        RawSelected = true;
    }
    
/*    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"AccSelVal"];
    if (tmpMode == 0){
        AccSelected = false;
    }
    else
    {
        AccSelected = true;
    }
*/    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"TargetSelVal"];
    if (tmpMode == 0){
        TargetSelected = false;
    }
    else
    {
        TargetSelected = true;
    }
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"DisplayIndicatorSelVal"];
    if (tmpMode == 0){
        DisplayIndicator = false;
    }
    else
    {
        DisplayIndicator = true;
    }
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"LimitsIndicatorSelVal"];
    if (tmpMode == 0){
        LimitsIndicator = false;
    }
    else
    {
        LimitsIndicator = true;
    }
    
    tmpMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"ChgColorSelVal"];
    if (tmpMode == 1){
        ChangeColor1 = 1;
        ChangeColor2 = 0;
        ChangeColor3 = 0;
        ChangeColor4 = 0;
    }
    else if (tmpMode == 2)
    {
        ChangeColor1 = 0;
        ChangeColor2 = 1;
        ChangeColor3 = 0;
        ChangeColor4 = 0;
    }
    else if (tmpMode == 3)
    {
        ChangeColor1 = 0;
        ChangeColor2 = 0;
        ChangeColor3 = 1;
        ChangeColor4 = 0;
    }
    else if (tmpMode == 4)
    {
        ChangeColor1 = 0;
        ChangeColor2 = 0;
        ChangeColor3 = 0;
        ChangeColor4 = 1;
    }
    else
    {
        ChangeColor1 = 1;
    }
    
    if (dispMode == 0)
        dispMode = 1;
    else
        dispMode = 0;
    
    /*
     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
     
     [accvertical setFrame:CGRectMake(133, 323, 66, 9)];
     [acc45deg setFrame:CGRectMake(571, 323, 66, 9)];
     [acchorizontal setFrame:CGRectMake(133, 323, 66, 9)];
     }
     else { //iPhone
     
     [accvertical setFrame:CGRectMake(62, 290, 66, 9)];
     [acc45deg setFrame:CGRectMake(207, 290, 66, 9)];
     [acchorizontal setFrame:CGRectMake(62, 290, 66, 9)];
     }
     */
    
    [self dispModeButton];
    
    
    if (!AlarmSelected)
        AlarmSelected = true;
    else
        AlarmSelected = false;
    
    [self AlarmButton];
    
    
    if (!GridSelected)
        GridSelected = true;
    else
        GridSelected = false;
    
    [self GridButton];
    
    
    if (!ShoesSelected)
        ShoesSelected = true;
    else
        ShoesSelected = false;
    
    [self ShoesButton];
    
    
    if (!ZeroFSRSelected)
        ZeroFSRSelected = true;
    else
        ZeroFSRSelected = false;
    
    [self ZeroFSRButton];
    
    
    if (!RawSelected)
        RawSelected = true;
    else
        RawSelected = false;
    
    [self RawButton];
    
    
    if (!AccSelected)
        AccSelected = true;
    else
        AccSelected = false;
    
    [self AccButton];
    
    
    if (!TargetSelected)
        TargetSelected = true;
    else
        TargetSelected = false;
    
    [self TargetSelectedButton];
    
    
    if (!DisplayIndicator)
        DisplayIndicator = true;
    else
        DisplayIndicator = false;
    
    [self DisplayIndicatorButton];
    
    
    if (!LimitsIndicator)
        LimitsIndicator = true;
    else
        LimitsIndicator = false;
    
    [self LimitsButton];
    
    
    if (ChangeColor1 == 1)
        [self ChangeColor1Button];
    
    
    if (ChangeColor2 == 1)
        [self ChangeColor2Button];
    
    
    if (ChangeColor3 == 1)
        [self ChangeColor3Button];
    
    
    if (ChangeColor4 == 1)
        [self ChangeColor4Button];
    
    
    
	OSStatus result = AudioSessionInitialize(NULL, NULL, ToneInterruptionListener, (__bridge void *)(self));
	if (result == kAudioSessionNoError)
	{
		UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
		AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
	}
	AudioSessionSetActive(true);
    
    
    CGAffineTransform trans1 = CGAffineTransformMakeRotation(M_PI * 1.0);
    //    lhorizontal.transform = trans1;
    lhorizontals.transform = trans1;
    horizontals2.transform = trans1;
    Lhind.transform = trans1;
    RLs.transform = trans1;
    
    //    CGAffineTransform trans2 = CGAffineTransformMakeRotation(M_PI * 1.75);
    acc45degs.transform = trans;
    //    acc45deg.transform = trans;
    
    /*
     Ltarget.hidden = YES;
     Rtarget.hidden = YES;
     verticals.hidden = YES;
     horizontals.hidden = YES;
     lverticals.hidden = YES;
     lhorizontals.hidden = YES;
     RLs.hidden = YES;
     verticals2.hidden = YES;
     horizontals2.hidden = YES;
     lverticals2.hidden = YES;
     lhorizontals2.hidden = YES;
     RLs2.hidden = YES;
     label4.hidden = YES;
     label5.hidden = YES;
     label8.hidden = YES;
     label9.hidden = YES;
     label21.hidden = YES;
     label22.hidden = YES;
     label23.hidden = YES;
     label24.hidden = YES;
     label25.hidden = YES;
     label26.hidden = YES;
     label30.hidden = YES;
     label31.hidden = YES;
     label32.hidden = YES;
     label33.hidden = YES;
     label34.hidden = YES;
     label35.hidden = YES;
     label36.hidden = YES;
     label37.hidden = YES;
     label38.hidden = YES;
     label39.hidden = YES;
     
     accelerometer.hidden = YES;
     accelerometerBatt.hidden = YES;
     label12.hidden = YES;
     label13.hidden = YES;
     label14.hidden = YES;
     label27.hidden = YES;
     label28.hidden = YES;
     label29.hidden = YES;
     puttT.hidden = YES;
     puttS.hidden = YES;
     arrow2.hidden = YES;
     arrow4.hidden = YES;
     acc45deg.hidden = YES;
     acc45degs.hidden = YES;
     acc45degs.enabled = NO;
     accvertical.hidden = YES;
     accverticals.hidden = YES;
     accverticals.enabled = NO;
     acchorizontal.hidden = YES;
     acchorizontals.hidden = YES;
     acchorizontals.enabled = NO;
     TSUIBatteryBarAcc.hidden = YES;
     TSUIScanForPeripheralsButtonAcc.hidden = YES;
     TSUIScanForPeripheralsButtonAcc.enabled = NO;
     //    TSUISpinnerAcc.hidden = YES;
     AccSelected = false;
     */
    
    basePlate.hidden = YES;
    graphview.hidden = YES;
    ChangeColor1Button.hidden = YES;
    ChangeColor2Button.hidden = YES;
    ChangeColor3Button.hidden = YES;
    ChangeColor4Button.hidden = YES;
    ClearDrawButton.hidden = YES;
    balanceView.hidden = YES;
    balanceTracking.hidden = YES;
    
    
    resultsView.hidden = YES;
    
    // Add the chart to the view controller
    //    [self.view addSubview:_chart];
    
    //    _chart.datasource = self;
    
    los1.hidden = YES;
    los2.hidden = YES;
    los3.hidden = YES;
    los4.hidden = YES;
    los5.hidden = YES;
    los6.hidden = YES;
    los7.hidden = YES;
    los8.hidden = YES;
    los1in.hidden = YES;
    los2in.hidden = YES;
    los3in.hidden = YES;
    los4in.hidden = YES;
    los5in.hidden = YES;
    los6in.hidden = YES;
    los7in.hidden = YES;
    los8in.hidden = YES;

    UIImage *feetButtonPressed = [UIImage imageNamed:@"balance_btn.png"];
    [feetButton setImage:feetButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:feetButton];
    
    UIImage *balanceButtonPressed = [UIImage imageNamed:@"feet_btn_up.png"];
    [balanceButton setImage:balanceButtonPressed forState:UIControlStateNormal];
    [self.view addSubview:balanceButton];
    
}


- (void)viewDidUnload
{
    //    [[NSUserDefaults standardUserDefaults] setFloat:[verticals value] forKey:@"sliderValue"];
    
    
    [self setTSUIBatteryBar:nil];
    [self setTSUIBatteryBarL:nil];
    [self setTSUIBatteryBarAcc:nil];
    //    [self setTSUIBatteryBarLabel:nil];
    [self setTSUILeftButton:nil];
    [self setTSUIRightButton:nil];
    [self setTSUIAccButton:nil];
    [self setTSUISpinner:nil];
    [self setTSUISpinnerL:nil];
    [self setTSUISpinnerAcc:nil];
    graphview = nil;
    
    AudioSessionSetActive(false);
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    switch(interfaceOrientation)
    {
        case UIInterfaceOrientationLandscapeLeft:
            return NO;
        case UIInterfaceOrientationLandscapeRight:
            return NO;
        default:
            return YES;
    }
}

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)TSUIScanForPeripheralsButton:(id)sender {
    if (t.activePeripheral)
        if(t.activePeripheral.isConnected){
            [[t CM] cancelPeripheralConnection:[t activePeripheral]];
            //            UIImage *rightConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
            //            [TSUIScanForPeripheralsButton setImage:rightConnected forState:UIControlStateNormal];
            //            [self.view addSubview:TSUIScanForPeripheralsButton];
            return;
        }
    
    if (t.peripherals)
        t.peripherals = nil;
    
    [t findBLEPeripherals:10];  //2
    [NSTimer scheduledTimerWithTimeInterval:(float)10.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats: NO];  //2.0
    
    [TSUISpinner startAnimating];
}

- (IBAction)TSUIScanForPeripheralsButtonL:(id)sender {
    if (t2.activePeripheral)
        if(t2.activePeripheral.isConnected){
            [[t2 CM] cancelPeripheralConnection:[t2 activePeripheral]];
            //            UIImage *leftConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
            //            [TSUIScanForPeripheralsButtonL setImage:leftConnected forState:UIControlStateNormal];
            //            [self.view addSubview:TSUIScanForPeripheralsButtonL];
            return;
        }
    
    if (t2.peripherals)
        t2.peripherals = nil;
    
    [t2 findBLEPeripherals:10];  //2
    [NSTimer scheduledTimerWithTimeInterval:(float)10.0 target:self selector:@selector(connectionTimerL:) userInfo:nil repeats:NO];  //2.0
    [TSUISpinnerL startAnimating];
}

- (IBAction)TSUIScanForPeripheralsButtonAcc:(id)sender {
    if (t3.activePeripheral)
        if(t3.activePeripheral.isConnected){
            [[t3 CM] cancelPeripheralConnection:[t3 activePeripheral]];
            //            UIImage *leftConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
            //            [TSUIScanForPeripheralsButtonL setImage:leftConnected forState:UIControlStateNormal];
            //            [self.view addSubview:TSUIScanForPeripheralsButtonL];
            return;
        }
    
    if (t3.peripherals)
        t3.peripherals = nil;
    
    [t3 findBLEPeripherals:10];  //2
    [NSTimer scheduledTimerWithTimeInterval:(float)10.0 target:self selector:@selector(connectionTimerAcc:) userInfo:nil repeats:NO];  //2.0
    [TSUISpinnerAcc startAnimating];
}

- (void) batteryIndicatorTimer:(NSTimer *)timer {
    TSUIBatteryBar.progress = t.batteryLevel / 100;
    [t readBattery:[t activePeripheral]];               // Read battery value of TroSensors again
    
    if(!t.activePeripheral.isConnected){
        UIImage *rightConnected = [UIImage imageNamed:@"ConnectBtn_Red.png"];
        [TSUIScanForPeripheralsButton setImage:rightConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButton];
        t.batteryLevel = 0;
        TSUIBatteryBar.progress = t.batteryLevel;
        [battTimer invalidate];
        battTimer = nil;
    }
    
}

- (void) batteryIndicatorTimerL:(NSTimer *)timer {
    TSUIBatteryBarL.progress = t2.batteryLevel / 100;
    [t2 readBattery:[t2 activePeripheral]];               // Read battery value of TroSensors again
    
    if(!t2.activePeripheral.isConnected){
        UIImage *leftConnected = [UIImage imageNamed:@"ConnectBtn_Red.png"];
        [TSUIScanForPeripheralsButtonL setImage:leftConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButtonL];
        t2.batteryLevel = 0;
        TSUIBatteryBarL.progress = t2.batteryLevel;
        [battTimerL invalidate];
        battTimerL = nil;
    }
    
}

- (void) batteryIndicatorTimerAcc:(NSTimer *)timer {
    TSUIBatteryBarAcc.progress = t3.batteryLevel / 100;
    [t3 readBattery:[t3 activePeripheral]];               // Read battery value of TroSensors again
    
    if(!t3.activePeripheral.isConnected){
        UIImage *accConnected = [UIImage imageNamed:@"ConnectBtn_Red.png"];
        [TSUIScanForPeripheralsButtonAcc setImage:accConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButtonAcc];
        t3.batteryLevel = 0;
        TSUIBatteryBarAcc.progress = t3.batteryLevel;
        [battTimerAcc invalidate];
        battTimerAcc = nil;
    }
    
}


// Method from TruSensorsDelegate, called when FSR values are updated
-(void) FSRValuesUpdated:(int)deviceNum fsr1:(char)fsr1 fsr2:(char)fsr2 fsr3:(char)fsr3 fsr4:(char)fsr4 {
    
    NSLog(@"FSRValuesUpdated: %d, fsr1: %d, fsr2: %d, fsr3: %d, fsr4: %d", deviceNum, fsr1, fsr2, fsr3, fsr4);
    if ( ZeroFSRSelected && (deviceNum != 3)) {
        if ( ZeroFSR1 && (deviceNum == 1) ) {
            fsr1ZeroVal1 = fsr1;
            fsr2ZeroVal1 = fsr2;
            fsr3ZeroVal1 = fsr3;
            fsr4ZeroVal1 = fsr4;
            ZeroFSR1 = false;
        }
        else if ( ZeroFSR2 && (deviceNum == 2) )
        {
            fsr1ZeroVal2 = fsr1;
            fsr2ZeroVal2 = fsr2;
            fsr3ZeroVal2 = fsr3;
            fsr4ZeroVal2 = fsr4;
            ZeroFSR2 = false;
        }
        if (deviceNum == 1){
            fsr1 -= fsr1ZeroVal1;
            if (fsr1 < 0) fsr1 = 0;
            fsr2 -= fsr2ZeroVal1;
            if (fsr2 < 0) fsr2 = 0;
            fsr3 -= fsr3ZeroVal1;
            if (fsr3 < 0) fsr3 = 0;
            fsr4 -= fsr4ZeroVal1;
            if (fsr4 < 0) fsr4 = 0;
        }
        else if (deviceNum ==2)
        {
            fsr1 -= fsr1ZeroVal2;
            if (fsr1 < 0) fsr1 = 0;
            fsr2 -= fsr2ZeroVal2;
            if (fsr2 < 0) fsr2 = 0;
            fsr3 -= fsr3ZeroVal2;
            if (fsr3 < 0) fsr3 = 0;
            fsr4 -= fsr4ZeroVal2;
            if (fsr4 < 0) fsr4 = 0;
        }
    }
    
    if (GraphOnly) {
        float flfsr1 = fsr1;
        float flfsr2 = fsr2;
        float flfsr3 = fsr3;
        float flfsr4 = fsr4;
        
        if(deviceNum == 1)
        {
            LC2 = flfsr1 + flfsr2;
            LC4 = flfsr3;
            LC1_2 = LC1 + LC2;
            LC3_4 = LC3 + LC4;
            LC1_3 = LC1 + LC3;
            LC2_4 = LC2 + LC4;
            
            if (LC1_2 == LC3_4) tverticalpos = 50;
            else {
                if (LC1_2 > LC3_4) tverticalpos = (((LC3_4 / LC1_2) / 2) * 100);
                else tverticalpos = ((((1 - (LC1_2 / LC3_4)) / 2) + 0.5) * 100);
            }
            if (LC1_3 == LC2_4) thorizontalpos = 50;
            else {
                if (LC1_3 > LC2_4) thorizontalpos = (((LC2_4 / LC1_3) / 2) * 100);
                else thorizontalpos = ((((1 - (LC1_3 / LC2_4)) / 2) + 0.5) * 100);
            }
            horizontalpos=(thorizontalpos * 5);
            
            verticalpos=(tverticalpos * 2.5);
            
            
            
            horizontalpos=(horizontalpos+135);
            
            verticalpos=((verticalpos *1.80)+140);
            
            
            //            target.center = CGPointMake (horizontalpos+135, (verticalpos *1.80)+140);
            target.center = CGPointMake (horizontalpos, verticalpos);
            
            [self->graphview setNeedsDisplay];
        }
        else if (deviceNum == 2)
        {
            LC1 = flfsr1 + flfsr2;
            LC3 = flfsr3;
            LC1_2 = LC1 + LC2;
            LC3_4 = LC3 + LC4;
            LC1_3 = LC1 + LC3;
            LC2_4 = LC2 + LC4;
            
            if (LC1_2 == LC3_4) tverticalpos = 50;
            else {
                if (LC1_2 > LC3_4) tverticalpos = (((LC3_4 / LC1_2) / 2) * 100);
                else tverticalpos = ((((1 - (LC1_2 / LC3_4)) / 2) + 0.5) * 100);
            }
            if (LC1_3 == LC2_4) thorizontalpos = 50;
            else {
                if (LC1_3 > LC2_4) thorizontalpos = (((LC2_4 / LC1_3) / 2) * 100);
                else thorizontalpos = ((((1 - (LC1_3 / LC2_4)) / 2) + 0.5) * 100);
            }
            horizontalpos=(thorizontalpos * 5);
            
            verticalpos=(tverticalpos * 2.5);
            
            
            
            horizontalpos=(horizontalpos+135);
            
            verticalpos=((verticalpos *1.80)+140);
            
            
//            target.center = CGPointMake (horizontalpos+135, (verticalpos *1.80)+140);
            target.center = CGPointMake (horizontalpos, verticalpos);
            
            [self->graphview setNeedsDisplay];
        }
        else
        {
            
        }
        
    }
    else {
        float flfsr1 = fsr1;
        float flfsr2 = fsr2;
        float flfsr3 = fsr3;
        float flfsr4 = fsr4;
        float hpercent;
        float vpercent;
        float tmpHorizontal2;
        float tmpVertical2;
        int tmpRL2;
        
        if(deviceNum == 1)
        {
            Final1X = fsr1;
            Final1Y = fsr2;
            Final1Z = fsr3;
            Final14 = fsr4;
            
            if(RawSelected){
                label1.text = [NSString stringWithFormat:@"%.f", flfsr1];
                label2.text = [NSString stringWithFormat:@"%.f", flfsr2];
                label3.text = [NSString stringWithFormat:@"%.f", flfsr3];
                labelR4.text = [NSString stringWithFormat:@"%.f", flfsr4];
            }
            else{
                label1.text = nil;
                label2.text = nil;
                label3.text = nil;
                labelR4.text = nil;
            }
            
            Rsum = flfsr1 + flfsr2 + flfsr3;
            
            vpercent = (flfsr2+flfsr1)/flfsr3;
            if (vpercent <  1.0)vpercent = (vpercent/2);
            else vpercent = 1 - ((flfsr3/(flfsr1+flfsr2))/2);
            //        vertical.progress = vpercent;
            Rvind.value = vpercent*100;
            
            hpercent = flfsr2/flfsr1;
            if (hpercent <  1.0)hpercent = (hpercent/2);
            else hpercent = 1 - ((flfsr1/flfsr2)/2);
            //        horizontal.progress = hpercent;
            Rhind.value = hpercent*100;
            
            
            label6.text = [NSString stringWithFormat:@"%.f", (hpercent*100)];
            tmpHorizontal2 = (100 - (hpercent*100));
            label41.text = [NSString stringWithFormat:@"%.f", tmpHorizontal2];
            label40.text = [NSString stringWithFormat:@"%.f", (vpercent*100)];
            tmpVertical2 = (100 - (vpercent*100));
            label7.text = [NSString stringWithFormat:@"%.f", tmpVertical2];
            
            if (Rsum == Lsum)RLSliderVal = 50;
            else {
                if (Lsum > Rsum)RLSliderVal = (((Rsum/Lsum) /2) *100);
                
                else RLSliderVal = ((((1-(Lsum/Rsum))/2)+0.5) *100);
            }
            RLindicator.value = RLSliderVal;
            label45.text = [NSString stringWithFormat:@"%d", RLSliderVal];
            tmpRL2 = (100 - RLSliderVal);
            label44.text = [NSString stringWithFormat:@"%d", tmpRL2];
            
            //        label12.text = [NSString stringWithFormat:@"%d", RLSliderVal];
            //        label13.text = [NSString stringWithFormat:@"%.f", Rsum];
 
//            float tmp_num = RLSliderVal;
//            los_3 = ((tmp_num-50)/50)*(patStance/2);
//            if (los_3 > los_3p) los_3p = los_3;
//            los3.text = [NSString stringWithFormat:@"%.1f", los_3p];

            
            if ((((vpercent*100)>vSliderVal2) || ((hpercent*100)>hSliderVal) || (tmpVertical2>vSliderVal) || (tmpHorizontal2>hSliderVal2) || ( RLSliderVal>RLsVal2) || ( tmpRL2>RLsVal)) && AlarmSelected) {
                //  red_alert;
                
                if (toneUnit)
                {
                }
                else
                {
                    [self createToneUnit];
                    
                    // Stop changing parameters on the unit
                    OSErr err = AudioUnitInitialize(toneUnit);
                    //                NSAssert1(err == noErr, @"Error initializing unit: %ld", err);
                    
                    // Start playback
                    err = AudioOutputUnitStart(toneUnit);
                    //                NSAssert1(err == noErr, @"Error starting unit: %ld", err);
                    
                    //                [selectedButton setTitle:NSLocalizedString(@"Stop", nil) forState:0];
                }
                fsr1tone = true;
                
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
                //            super.view.backgroundColor = [UIColor redColor];
//                alarm.hidden = NO;
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    drawing.hidden = YES;
            }
            else {
                //  normal_alert;
                
                if (toneUnit && !fsr2tone && !acctone && AlarmSelected)
                {
                    AudioOutputUnitStop(toneUnit);
                    AudioUnitUninitialize(toneUnit);
                    AudioComponentInstanceDispose(toneUnit);
                    toneUnit = nil;
                    
                    //                [selectedButton setTitle:NSLocalizedString(@"Play", nil) forState:0];
                }
                
                //            super.view.backgroundColor = [UIColor lightGrayColor];
//                alarm.hidden = YES;
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    if (! device4s)
                        drawing.hidden = NO;
                fsr1tone = false;
                
                //  super.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
            }
            
            LC2 = flfsr1 + flfsr2;
            LC4 = flfsr3;
            LC1_2 = LC1 + LC2;
            LC3_4 = LC3 + LC4;
            LC1_3 = LC1 + LC3;
            LC2_4 = LC2 + LC4;
            
            if (LC1_2 == LC3_4) tverticalpos = 50;
            else {
                if (LC1_2 > LC3_4) tverticalpos = (((LC3_4 / LC1_2) / 2) * 100);
                else tverticalpos = ((((1 - (LC1_2 / LC3_4)) / 2) + 0.5) * 100);
            }
            if (LC1_3 == LC2_4) thorizontalpos = 50;
            else {
                if (LC1_3 > LC2_4) thorizontalpos = (((LC2_4 / LC1_3) / 2) * 100);
                else thorizontalpos = ((((1 - (LC1_3 / LC2_4)) / 2) + 0.5) * 100);
            }
            //        label15.text = [NSString stringWithFormat:@"%.f", thorizontalpos];
            //        label16.text = [NSString stringWithFormat:@"%.f", tverticalpos];
            
            //        horizontalpos=(thorizontalpos * 5)-(width/2);
            //horizontalpos=(thorizontalpos * 5);
            
            //        if (horizontalpos > (width/2))
            //            horizontalpos = (width/2);
            
            //        verticalpos=(tverticalpos * 2.5)-(height/2);
            //verticalpos=(tverticalpos * 2.5);
            
            
            
            float tmp_num = thorizontalpos;
            los_3 = ((tmp_num-50)/50)*(patStance/1.7);
            if (los_3 > los_3p) los_3p = los_3;
            los3.text = [NSString stringWithFormat:@"%.1f", los_3p];
            
            tmp_num = 100-tverticalpos;
            los_1 = ((tmp_num-50)/50)*(patHeight/8);
            if (los_1 > los_1p) los_1p = los_1;
            los1.text = [NSString stringWithFormat:@"%.1f", los_1p];
            
            tmp_num = tverticalpos;
            los_5 = ((tmp_num-50)/50)*(patHeight/8);
            if (los_5 > los_5p) los_5p = los_5;
            los5.text = [NSString stringWithFormat:@"%.1f", los_5p];

            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                horizontalpos=(thorizontalpos * 5);
                verticalpos=(tverticalpos * 2.5);
                
                
                
//                verticalpos=verticalpos *1.80-iOS_offset-20;
                verticalpos=verticalpos *1.60-iOS_offset;
                
                
            }
            else { //iPhone
                horizontalpos=(thorizontalpos * 2);
                verticalpos=(tverticalpos * 1.6);
            }
            
            if (TargetSelected) {
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                    //                horizontalpos=(thorizontalpos * 5);
                    //                verticalpos=(tverticalpos * 2.5);
                    //                    target.center = CGPointMake (horizontalpos+135, (verticalpos *1.80)+140);
//                    target.center = CGPointMake (horizontalpos+135, (verticalpos *1.80)+150-iOS_offset);//+160
                    target.center = CGPointMake (horizontalpos+135, verticalpos+150+40);//+160
                }
                else { //iPhone
                    //                horizontalpos=(thorizontalpos * 2.2);
                    //                verticalpos=(tverticalpos * 2);
                    //                    target.center = CGPointMake (horizontalpos+61, (verticalpos*1.25+110));
                    target.center = CGPointMake (horizontalpos+61, (verticalpos*1.25+130-iOS_offset));//+130
                }
            }
            
            if (ShoesSelected) {
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                    Rtarget.center = CGPointMake ((int)(hpercent * 100)+503, (int)((1-vpercent) * 295)+ 220-iOS_offset);
                    //                    Rtarget.center = CGPointMake ((int)(hpercent * 100)+503, (int)((1-vpercent) * 295)+ 240);
                }
                else { //iPhone
                    //                    Rtarget.center = CGPointMake ((int)(hpercent * 75)+185, (int)((1-vpercent) * 180)+ 120);
                    Rtarget.center = CGPointMake ((int)(hpercent * 75)+185, (int)((1-vpercent) * 180)+ 140-iOS_offset);
                }
            }
            //        if (verticalpos > (height/2))
            //            verticalpos = (height/2);
            
            //        if (StartDraw == 1) [self->graphview setNeedsDisplay];
            [self->graphview setNeedsDisplay];
        }
        else if(deviceNum == 2)
        {
            Final2X = fsr1;
            Final2Y = fsr2;
            Final2Z = fsr3;
            Final24 = fsr4;
            
            if(RawSelected){
                labelL1.text = [NSString stringWithFormat:@"%.f", flfsr1];
                labelL2.text = [NSString stringWithFormat:@"%.f", flfsr2];
                labelL3.text = [NSString stringWithFormat:@"%.f", flfsr3];
                labelL4.text = [NSString stringWithFormat:@"%.f", flfsr4];
            }
            else{
                labelL1.text = nil;
                labelL2.text = nil;
                labelL3.text = nil;
                labelL4.text = nil;
            }
            
            Lsum = flfsr1 + flfsr2 + flfsr3;
            
            vpercent = (flfsr2+flfsr1)/flfsr3;
            if (vpercent <  1.0)vpercent = (vpercent/2);
            else vpercent = 1 - ((flfsr3/(flfsr1+flfsr2))/2);
            //        lvertical.progress = vpercent;
            Lvind.value=vpercent*100;
            
            hpercent = flfsr2/flfsr1;
            if (hpercent <  1.0)hpercent = (hpercent/2);
            else hpercent = 1 - ((flfsr1/flfsr2)/2);
            //        lhorizontal.progress = hpercent;
            Lhind.value = hpercent*100;
            
            label10.text = [NSString stringWithFormat:@"%.f", (hpercent*100)];
            tmpHorizontal2 = (100 - (hpercent*100));
            label42.text = [NSString stringWithFormat:@"%.f", tmpHorizontal2];
            label43.text = [NSString stringWithFormat:@"%.f", (vpercent*100)];
            tmpVertical2 = (100 - (vpercent*100));
            label11.text = [NSString stringWithFormat:@"%.f", tmpVertical2];
            
            if (Rsum == Lsum)RLSliderVal = 50;
            else {
                if (Lsum > Rsum)RLSliderVal = (((Rsum/Lsum) /2) *100);
                
                else RLSliderVal = ((((1-(Lsum/Rsum))/2)+0.5) *100);
            }
            RLindicator.value = RLSliderVal;
            label45.text = [NSString stringWithFormat:@"%d", RLSliderVal];
            tmpRL2 = (100 - RLSliderVal);
            label44.text = [NSString stringWithFormat:@"%d", tmpRL2];
            
            //        label12.text = [NSString stringWithFormat:@"%d", RLSliderVal];
            //        label14.text = [NSString stringWithFormat:@"%.f", Lsum];
            
            
//            float tmp_num = tmpRL2;
//            los_7 = ((tmp_num-50)/50)*(patStance/2);
//            if (los_7 > los_7p) los_7p = los_7;
//            los7.text = [NSString stringWithFormat:@"%.1f", los_7p];

            
            if ((((vpercent*100)>lvSliderVal2) || ((hpercent*100)>lhSliderVal) || (tmpVertical2>lvSliderVal) || (tmpHorizontal2>lhSliderVal2) || ( RLSliderVal>RLsVal2) || ( tmpRL2>RLsVal)) && AlarmSelected) {
                //  red_alert;
                
                if (toneUnit)
                {
                }
                else
                {
                    [self createToneUnit];
                    
                    // Stop changing parameters on the unit
                    OSErr err = AudioUnitInitialize(toneUnit);
                    //                NSAssert1(err == noErr, @"Error initializing unit: %ld", err);
                    
                    // Start playback
                    err = AudioOutputUnitStart(toneUnit);
                    //                NSAssert1(err == noErr, @"Error starting unit: %ld", err);
                    
                    //                [selectedButton setTitle:NSLocalizedString(@"Stop", nil) forState:0];
                }
                fsr2tone = true;
                
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
                //            super.view.backgroundColor = [UIColor redColor];
//                alarm.hidden = NO;
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    drawing.hidden = YES;
            }
            else {
                //  normal_alert;
                
                if (toneUnit && !fsr1tone && !acctone && AlarmSelected)
                {
                    AudioOutputUnitStop(toneUnit);
                    AudioUnitUninitialize(toneUnit);
                    AudioComponentInstanceDispose(toneUnit);
                    toneUnit = nil;
                    
                    //                [selectedButton setTitle:NSLocalizedString(@"Play", nil) forState:0];
                }
                
                //            super.view.backgroundColor = [UIColor lightGrayColor];
//                alarm.hidden = YES;
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    if (! device4s)
                        drawing.hidden = NO;
                fsr2tone = false;
                
                //  super.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
            }
            
            LC1 = flfsr1 + flfsr2;
            LC3 = flfsr3;
            LC1_2 = LC1 + LC2;
            LC3_4 = LC3 + LC4;
            LC1_3 = LC1 + LC3;
            LC2_4 = LC2 + LC4;
            
            if (LC1_2 == LC3_4) tverticalpos = 50;
            else {
                if (LC1_2 > LC3_4) tverticalpos = (((LC3_4 / LC1_2) / 2) * 100);
                else tverticalpos = ((((1 - (LC1_2 / LC3_4)) / 2) + 0.5) * 100);
            }
            if (LC1_3 == LC2_4) thorizontalpos = 50;
            else {
                if (LC1_3 > LC2_4) thorizontalpos = (((LC2_4 / LC1_3) / 2) * 100);
                else thorizontalpos = ((((1 - (LC1_3 / LC2_4)) / 2) + 0.5) * 100);
            }
            //        label15.text = [NSString stringWithFormat:@"%.f", thorizontalpos];
            //        label16.text = [NSString stringWithFormat:@"%.f", tverticalpos];
            
            //        horizontalpos=(thorizontalpos * 5)-(width/2);
            //        horizontalpos=(thorizontalpos * 5);
            
            //        if (horizontalpos > (width/2))
            //            horizontalpos = (width/2);
            
            //        verticalpos=(tverticalpos * 2.5)-(height/2);
            //        verticalpos=(tverticalpos * 2.5);

            
            float tmp_num = 100-thorizontalpos;
            los_7 = ((tmp_num-50)/50)*(patStance/1.7);
            if (los_7 > los_7p) los_7p = los_7;
            los7.text = [NSString stringWithFormat:@"%.1f", los_7p];
            
            tmp_num = 100-tverticalpos;
            los_1 = ((tmp_num-50)/50)*(patHeight/8);
            if (los_1 > los_1p) los_1p = los_1;
            los1.text = [NSString stringWithFormat:@"%.1f", los_1p];
            
            tmp_num = tverticalpos;
            los_5 = ((tmp_num-50)/50)*(patHeight/8);
            if (los_5 > los_5p) los_5p = los_5;
            los5.text = [NSString stringWithFormat:@"%.1f", los_5p];

            
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                horizontalpos=(thorizontalpos * 5);
                verticalpos=(tverticalpos * 2.5);
                
                
//                verticalpos=verticalpos *1.80-iOS_offset-20;
                verticalpos=verticalpos *1.60-iOS_offset;

            }
            else { //iPhone
                horizontalpos=(thorizontalpos * 2);
                verticalpos=(tverticalpos * 1.6);
            }
            
            if (TargetSelected) {
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                    //                horizontalpos=(thorizontalpos * 5);
                    //                verticalpos=(tverticalpos * 2.5);
                    //                    target.center = CGPointMake (horizontalpos+135, (verticalpos *1.80)+140);
//                    target.center = CGPointMake (horizontalpos+135, (verticalpos *1.80)+150-iOS_offset);//+160
                    target.center = CGPointMake (horizontalpos+135, verticalpos+150+40);//+160
                    
                }
                else { //iPhone
                    //                horizontalpos=(thorizontalpos * 2.2);
                    //                verticalpos=(tverticalpos * 2);
                    //                    target.center = CGPointMake (horizontalpos+61, (verticalpos*1.25+110));
                    target.center = CGPointMake (horizontalpos+61, (verticalpos*1.25+130-iOS_offset));//+130
                }
            }
            
            if (ShoesSelected) {
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { //iPad
                    Ltarget.center = CGPointMake ((int)((1-hpercent) * 100)+165, (int)((1-vpercent) * 295)+ 220-iOS_offset);
                    //                    Ltarget.center = CGPointMake ((int)((1-hpercent) * 100)+165, (int)((1-vpercent) * 295)+ 240);
                }
                else { //iPhone
                    //                    Ltarget.center = CGPointMake ((int)(1-hpercent * 75)+135, (int)((1-vpercent) * 180)+ 120);
                    Ltarget.center = CGPointMake ((int)(1-hpercent * 75)+135, (int)((1-vpercent) * 180)+ 140-iOS_offset);
                }
            }
            
            //        if (verticalpos > (height/2))
            //            verticalpos = (height/2);
            
            //        if (StartDraw == 1) [self->graphview setNeedsDisplay];
            [self->graphview setNeedsDisplay];
        }
        else {
            Final3X = fsr1;
            Final3Y = fsr2;
            Final3Z = fsr3;
            
            acc45deg.progress = (float)((abs(fsr2 - oldy)) * 4) / 100;
            acchorizontal.progress = (float)((abs(fsr3 - oldz)) * 4) / 100;
            accvertical.progress = (float)((abs(fsr1 - oldx)) * 4) / 100;
            //        accvertical.progress = (float)((abs(fsr2 - oldy)) * 4) / 100;
            //        acchorizontal.progress = (float)((abs(fsr3 - oldz)) * 4) / 100;
            //        acc45deg.progress = (float)((abs(fsr1 - oldx)) * 4) / 100;
            
            if (((abs((fsr3 - oldz)*4)) > acchSliderVal || (abs((fsr2-oldy)*4))>accvSliderVal || (abs((fsr1-oldx)*4))>acc45degSliderVal)&& AlarmSelected){
                // red_alert;
                /*
                 if (toneUnit)
                 {
                 }
                 else
                 {
                 [self createToneUnit];
                 
                 // Stop changing parameters on the unit
                 OSErr err = AudioUnitInitialize(toneUnit);
                 //                NSAssert1(err == noErr, @"Error initializing unit: %ld", err);
                 
                 // Start playback
                 err = AudioOutputUnitStart(toneUnit);
                 //                NSAssert1(err == noErr, @"Error starting unit: %ld", err);
                 
                 //                [selectedButton setTitle:NSLocalizedString(@"Stop", nil) forState:0];
                 }
                 acctone = true;
                 */
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
                //super.view.backgroundColor = [UIColor redColor];
                alarm.hidden = NO;
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    drawing.hidden = YES;
                AlarmLine = 1;
            }
            else {
                //normal_alert;
                /*
                 if (toneUnit && !fsr1tone && !fsr2tone)
                 {
                 AudioOutputUnitStop(toneUnit);
                 AudioUnitUninitialize(toneUnit);
                 AudioComponentInstanceDispose(toneUnit);
                 toneUnit = nil;
                 
                 //                [selectedButton setTitle:NSLocalizedString(@"Play", nil) forState:0];
                 }
                 */
                //super.view.backgroundColor = [UIColor lightGrayColor];
                alarm.hidden = YES;
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    if (! device4s)
                        drawing.hidden = NO;
                acctone = false;
                
            }
            oldx = fsr1;
            oldy = fsr2;
            oldz = fsr3;
            old4 = fsr4;
        }
    }
}

// Method from TruSensorsDelegate, called when key values are updated
-(void) keyValuesUpdated:(int)deviceNum sw:(char)sw {
    printf("Key values updated ! \r\n");
    
    if(deviceNum == 1)
    {
        if (sw & 0x1)
            [TSUILeftButton setOn:TRUE];
        else
            [TSUILeftButton setOn: FALSE];
        
        if (sw & 0x2)
            [TSUIRightButton setOn: TRUE];
        else
            [TSUIRightButton setOn: FALSE];
    }
    /* else
     {
     if (sw & 0x1)
     [TSUILeftButton setOn:TRUE];
     else
     [TSUILeftButton setOn: FALSE];
     
     if (sw & 0x2)
     [TSUIRightButton setOn: TRUE];
     else
     [TSUIRightButton setOn: FALSE];
     }*/
}

-(const char *) CBUUIDToString:(CBUUID*)UUID {
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}

//Method from TruSensorsDelegate, called when TruSensors has been found and all services have been discovered
-(void) TruSensorsReady:(int) deviceNum {
    
    if(deviceNum == 1)
    {
        // Start battery indicator timer, calls batteryIndicatorTimer method every 2 seconds
        battTimer = [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(batteryIndicatorTimer:) userInfo:nil repeats:YES];
        
        [t enableFSR:[t activePeripheral]];             // Enable fsr (if found)
        [t enableButtons:[t activePeripheral]];         // Enable button service (if found)
        [t enableTXPower:[t activePeripheral]];         // Enable TX power service (if found)
        [TSUISpinner stopAnimating];
        UIImage *rightConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
        [TSUIScanForPeripheralsButton setImage:rightConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButton];
    }
    else if(deviceNum == 2)
    {
        // Start battery indicator timer, calls batteryIndicatorTimer method every 2 seconds
        battTimerL = [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(batteryIndicatorTimerL:) userInfo:nil repeats:YES];
        
        [t2 enableFSR:[t2 activePeripheral]];             // Enable fsr (if found)
        [t2 enableButtons:[t2 activePeripheral]];         // Enable button service (if found)
        [t2 enableTXPower:[t2 activePeripheral]];         // Enable TX power service (if found)
        [TSUISpinnerL stopAnimating];
        UIImage *leftConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
        [TSUIScanForPeripheralsButtonL setImage:leftConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButtonL];
    }
    else{
        // Start battery indicator timer, calls batteryIndicatorTimer method every 2 seconds
        battTimerAcc = [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(batteryIndicatorTimerAcc:) userInfo:nil repeats:YES];
        
        [t3 enableFSR:[t3 activePeripheral]];             // Enable fsr (if found)
        [t3 enableButtons:[t3 activePeripheral]];         // Enable button service (if found)
        [t3 enableTXPower:[t3 activePeripheral]];         // Enable TX power service (if found)
        [TSUISpinnerAcc stopAnimating];
        UIImage *accConnected = [UIImage imageNamed:@"ConnectBtn_Green.png"];
        [TSUIScanForPeripheralsButtonAcc setImage:accConnected forState:UIControlStateNormal];
        [self.view addSubview:TSUIScanForPeripheralsButtonAcc];
    }
}

//Method from TruSensorsDelegate, called when TX powerlevel values are updated
-(void) TXPwrLevelUpdated:(int)deviceNum TXPwr:(char)TXPwr {
    //if(deviceNum == 1)
}

// Called when scan period is over to connect to the first found peripheral
-(void) connectionTimer:(NSTimer *)timer {
    if(t.peripherals.count > 0)
    {
        [t connectPeripheral:[t.peripherals objectAtIndex:0]];
    }
    else [TSUISpinner stopAnimating];
}

// Called when scan period is over to connect to the first found peripheral
-(void) connectionTimerL:(NSTimer *)timer {
    if(t2.peripherals.count > 0)
    {
        [t2 connectPeripheral:[t2.peripherals objectAtIndex:0]];
    }
    else [TSUISpinnerL stopAnimating];
}

// Called when scan period is over to connect to the first found peripheral
-(void) connectionTimerAcc:(NSTimer *)timer {
    if(t3.peripherals.count > 0)
    {
        [t3 connectPeripheral:[t3.peripherals objectAtIndex:0]];
    }
    else [TSUISpinnerAcc stopAnimating];
}



- (IBAction)TSUISoundBuzzerButton:(id)sender {
    [t soundBuzzer:0x02 p:[t activePeripheral]]; //Sound buzzer with 0x02 as data value
}

/*-(void)dealloc {
 [horizontals release];
 [verticals release];
 [super dealloc];
 }*/


- (int)numberOfSeriesInSChart:(ShinobiChart *)chart {
    return 11;
}

-(SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(int)index {
    
    SChartLineSeries *lineSeries = [[SChartLineSeries alloc] init];
    
    //    lineSeries.style.areaColor = [UIColor redColor];
    
    // the first series is a cosine curve, the second is a sine curve
    if (index == 0) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 1"];
        lineSeries.style.lineColor = [UIColor maraschinoCrayonColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 1) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 2"];
        lineSeries.style.lineColor = [UIColor asparagusCrayonColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 2) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 3"];
        lineSeries.style.lineColor = [UIColor tangerineCrayonColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 3) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 4"];
        //        lineSeries.style.lineColor = [UIColor cyanColor];
        lineSeries.style.lineColor = [UIColor colorWithRed:0x49/255.0f  green:0xcd/255.0f  blue:0xa7/255.0f  alpha:1.0]; //RGB
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 4) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 5"];
        lineSeries.style.lineColor = [UIColor blueColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 5) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 6"];
        lineSeries.style.lineColor = [UIColor greenColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 6) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 7"];
        lineSeries.style.lineColor = [UIColor purpleColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 7) {
        lineSeries.title = [NSString stringWithFormat:@"fsr 8"];
        lineSeries.style.lineColor = [UIColor colorWithRed:0xB0/255.0f  green:0xC4/255.0f  blue:0xDE/255.0f  alpha:1.0];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 8) {
        lineSeries.title = [NSString stringWithFormat:@"acc 1"];
        lineSeries.style.lineColor = [UIColor magentaColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else if (index == 9) {
        lineSeries.title = [NSString stringWithFormat:@"acc 2"];
        lineSeries.style.lineColor = [UIColor cyanColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    } else {
        lineSeries.title = [NSString stringWithFormat:@"acc 3"];
        lineSeries.style.lineColor = [UIColor grayColor];
        //        lineSeries.style.lineWidth = [NSNumber numberWithDouble:2];
    }
    
    //    lineSeries.lineColor = [UIColor redColor];
    lineSeries.crosshairEnabled = YES;
    //    self->_chart1.crosshair.interpolatePoints = NO;
    
    return lineSeries;
}

- (int)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(int)seriesIndex {
    return ((rowArray.count)-1);
}

- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(int)dataIndex forSeriesAtIndex:(int)seriesIndex {
    
    SChartDataPoint *datapoint = [[SChartDataPoint alloc] init];
    
    // both functions share the same x-values
    //    double xValue = dataIndex / 10.0;
    double xValue = dataIndex / 30.0; // was 30.0
    
    datapoint.xValue = [NSNumber numberWithDouble:xValue];
    
    // compute the y-value for each series
    if (seriesIndex == 0) {
        if (devPlot1){
            NSString *stringValue = [firstDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 1) {
        if (devPlot1){
            NSString *stringValue = [secondDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 2) {
        if (devPlot1){
            NSString *stringValue = [thirdDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 3) {
        if (devPlot1){
            NSString *stringValue = [fourthDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 4) {
        if (devPlot2){
            NSString *stringValue = [fifthDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 5) {
        if (devPlot2){
            NSString *stringValue = [sixthDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 6) {
        if (devPlot2){
            NSString *stringValue = [seventhDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 7) {
        if (devPlot2){
            NSString *stringValue = [eighthDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 8) {
        if (devPlot3){
            NSString *stringValue = [ninthDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else if (seriesIndex == 9) {
        if (devPlot3){
            NSString *stringValue = [tenthDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    } else {
        if (devPlot3){
            NSString *stringValue = [eleventhDevArray[dataIndex] stringValue];
            datapoint.yValue = @(stringValue.floatValue);
        }
        else
            datapoint.yValue = [NSNumber numberWithDouble:0];
    }
    /*
     if (seriesIndex == 0) {
     if (devPlot1)
     datapoint.yValue = firstDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else if (seriesIndex == 1) {
     if (devPlot1)
     datapoint.yValue = secondDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else if (seriesIndex == 2) {
     if (devPlot1)
     datapoint.yValue = thirdDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else if (seriesIndex == 3) {
     if (devPlot2)
     datapoint.yValue = fourthDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else if (seriesIndex == 4) {
     if (devPlot2)
     datapoint.yValue = fifthDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else if (seriesIndex == 5) {
     if (devPlot2)
     datapoint.yValue = sixthDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else if (seriesIndex == 6) {
     if (devPlot3)
     datapoint.yValue = seventhDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else if (seriesIndex == 7) {
     if (devPlot3)
     datapoint.yValue = eighthDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     } else {
     if (devPlot3)
     datapoint.yValue = ninthDevArray[dataIndex];
     else
     datapoint.yValue = [NSNumber numberWithDouble:0];
     }
     */
    return datapoint;
}


- (void)loadChartData {
    // load data into an array
    
    filenames = [self getAllPatientTests];
    
    if (filenames.count > 0)
    {
        
        Test *patientTest = filenames[openTestIndex];
        
        rowArray = [[NSArray alloc] init];
        rowArray = [NSKeyedUnarchiver unarchiveObjectWithData:patientTest.testData];
        
        
        //Add and initialize arrays here, this code is for *.csv files where you know the data you want.
        firstDevArray = [[NSMutableArray alloc]init];
        secondDevArray = [[NSMutableArray alloc]init];
        thirdDevArray = [[NSMutableArray alloc]init];
        fourthDevArray = [[NSMutableArray alloc]init];
        fifthDevArray = [[NSMutableArray alloc]init];
        sixthDevArray = [[NSMutableArray alloc]init];
        seventhDevArray = [[NSMutableArray alloc]init];
        eighthDevArray = [[NSMutableArray alloc]init];
        ninthDevArray = [[NSMutableArray alloc]init];
        tenthDevArray = [[NSMutableArray alloc]init];
        eleventhDevArray = [[NSMutableArray alloc]init];
        
        for(int i = 0; i < ([rowArray count] - 1); i++){
            
            NSArray *tempArray = [rowArray objectAtIndex:i];
            
            [firstDevArray addObject:[tempArray objectAtIndex:0]];
            [secondDevArray addObject:[tempArray objectAtIndex:1]];
            [thirdDevArray addObject:[tempArray objectAtIndex:2]];
            [fourthDevArray addObject:[tempArray objectAtIndex:3]];
            [fifthDevArray addObject:[tempArray objectAtIndex:4]];
            [sixthDevArray addObject:[tempArray objectAtIndex:5]];
            [seventhDevArray addObject:[tempArray objectAtIndex:6]];
            [eighthDevArray addObject:[tempArray objectAtIndex:7]];
            [ninthDevArray addObject:[tempArray objectAtIndex:8]];
            [tenthDevArray addObject:[tempArray objectAtIndex:9]];
            [eleventhDevArray addObject:[tempArray objectAtIndex:10]];
        }
    }
    
    
}

-(void)generatePDF:(NSString *)filePath
{
    UIGraphicsBeginPDFContextToFile(filePath,CGRectZero,nil);
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0,0,850,1100),nil);
    [self drawGraph];
    UIGraphicsEndPDFContext();
    
}

-(void)drawGraph
{
    CGRect imageRect = CGRectMake(110, 120, 610, 600);
    UIGraphicsBeginImageContextWithOptions(self->_chart1.bounds.size, NO, 0.0);
    [self->_chart1 drawViewHierarchyInRect:self->_chart1.bounds afterScreenUpdates:NO];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [image drawInRect:imageRect];
    
    /*
     CGContextRef context = UIGraphicsGetCurrentContext();
     CGRect rect = CGRectMake(0, 0, 500, 500);
     CGContextSetFillColorWithColor(context, [[UIColor greenColor] CGColor]);
     CGContextFillRect(context, rect);
     */
    
}


@end

