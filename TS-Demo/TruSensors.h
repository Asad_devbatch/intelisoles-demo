//
//  TruSensors.h
//  TruSensors
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreBluetooth/CBService.h>
#import "TruSensorsDefines.h"


@protocol TruSensorsDelegate
@optional
-(void) TruSensorsReady:(int)deviceNum;
@required
-(void) FSRValuesUpdated:(int)deviceNum fsr1:(char)fsr1 fsr2:(char)fsr2 fsr3:(char)fsr3 fsr4:(char)fsr4;
-(void) keyValuesUpdated:(int)deviceNum sw:(char)sw;
-(void) TXPwrLevelUpdated:(int)deviceNum TXPwr:(char)TXPwr;
@end

@interface TruSensors : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate> {
}


@property (nonatomic)   float batteryLevel;
@property (nonatomic)   float LbatteryLevel;
@property (nonatomic)   float AccbatteryLevel;
@property (nonatomic)   BOOL key1;
@property (nonatomic)   BOOL key2;
@property (nonatomic)   BOOL key3;
@property (nonatomic)   char fsr1;
@property (nonatomic)   char fsr2;
@property (nonatomic)   char fsr3;
@property (nonatomic)   char fsr4;
@property (nonatomic)   char Lfsr1;
@property (nonatomic)   char Lfsr2;
@property (nonatomic)   char Lfsr3;
@property (nonatomic)   char Lfsr4;
@property (nonatomic)   char Acc1;
@property (nonatomic)   char Acc2;
@property (nonatomic)   char Acc3;
@property (nonatomic)   char TXPwrLevel;
@property (nonatomic)   int deviceNum;


@property (nonatomic,assign) id <TruSensorsDelegate> delegate;
@property (strong, nonatomic)  NSMutableArray *peripherals;
@property (strong, nonatomic) CBCentralManager *CM;
@property (strong, nonatomic) CBPeripheral *activePeripheral;

-(void) soundBuzzer:(Byte)buzVal p:(CBPeripheral *)p;
-(void) readBattery:(CBPeripheral *)p;
-(void) enableFSR:(CBPeripheral *)p;
-(void) disableFSR:(CBPeripheral *)p;
-(void) enableButtons:(CBPeripheral *)p;
-(void) disableButtons:(CBPeripheral *)p;
-(void) enableTXPower:(CBPeripheral *)p;
-(void) disableTXPower:(CBPeripheral *)p;


-(void) writeValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID  p:(CBPeripheral *)p data:(NSData *)data;
-(void) readValue: (int)serviceUUID characteristicUUID:(int)characteristicUUID  p:(CBPeripheral *)p;
-(void) notification:(int)serviceUUID characteristicUUID:(int)characteristicUUID  p:(CBPeripheral *)p on:(BOOL)on;


-(UInt16) swap:(UInt16) s;
-(int) controlSetup:(int) s;
-(int) findBLEPeripherals:(int) timeout;
-(const char *) centralManagerStateToString:(int)state;
-(void) scanTimer:(NSTimer *)timer;
-(void) printKnownPeripherals;
-(void) printPeripheralInfo:(CBPeripheral*)peripheral;
-(void) connectPeripheral:(CBPeripheral *)peripheral;

-(void) getAllServicesFromTruSensors:(CBPeripheral *)p;
-(void) getAllCharacteristicsFromTruSensors:(CBPeripheral *)p;
-(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p;
-(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service;
-(const char *) UUIDToString:(CFUUIDRef) UUID;
-(const char *) CBUUIDToString:(CBUUID *) UUID;
-(int) compareCBUUID:(CBUUID *) UUID1 UUID2:(CBUUID *)UUID2;
-(int) compareCBUUIDToInt:(CBUUID *) UUID1 UUID2:(UInt16)UUID2;
-(UInt16) CBUUIDToInt:(CBUUID *) UUID;
-(int) UUIDSAreEqual:(CFUUIDRef)u1 u2:(CFUUIDRef)u2;



@end
