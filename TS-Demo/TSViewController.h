//
//  TSViewController.h
//  TruSensors
//

#import <UIKit/UIKit.h>
#import "TruSensors.h"
#import <MessageUI/MessageUI.h>
#import <AudioUnit/AudioUnit.h>
#import "Patient.h"
#import "Test.h"

extern UInt8 Final1X;
extern UInt8 Final1Y;
extern UInt8 Final1Z;
extern UInt8 Final14;
extern UInt8 Final2X;
extern UInt8 Final2Y;
extern UInt8 Final2Z;
extern UInt8 Final24;
extern UInt8 Final3X;
extern UInt8 Final3Y;
extern UInt8 Final3Z;

extern float los_1, los_2, los_3, los_4, los_5, los_6, los_7, los_8;
extern float los_1p, los_2p, los_3p, los_4p, los_5p, los_6p, los_7p, los_8p;

@class Draw;

NSFileHandle *fileHandle;

//char oldx = 0, oldy = 0, oldz = 0;

@interface TSViewController : UIViewController <TruSensorsDelegate,
MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource> {
    
    NSTimer *battTimer;
    NSTimer *battTimerL;
    NSTimer *battTimerAcc;
    
    __weak IBOutlet Draw *graphview;
    
    //    IBOutlet Draw *view1;
    TruSensors *t; //TruSensors class (private)
    TruSensors *t2;
    TruSensors *t3;
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    IBOutlet UILabel *labelR4;
    IBOutlet UILabel *labelL1;
    IBOutlet UILabel *labelL2;
    IBOutlet UILabel *labelL3;
    IBOutlet UILabel *labelL4;
    IBOutlet UILabel *label4;
    IBOutlet UILabel *label5;
    IBOutlet UILabel *label6;
    IBOutlet UILabel *label7;
    IBOutlet UILabel *label8;
    IBOutlet UILabel *label9;
    IBOutlet UILabel *label10;
    IBOutlet UILabel *label11;
    IBOutlet UILabel *label12;
    IBOutlet UILabel *label13;
    IBOutlet UILabel *label14;
    IBOutlet UILabel *label15;
    IBOutlet UILabel *label16;
    IBOutlet UILabel *label17;
    IBOutlet UILabel *label18;
    IBOutlet UILabel *label19;
    IBOutlet UILabel *label20;
    IBOutlet UILabel *label21;
    IBOutlet UILabel *label22;
    IBOutlet UILabel *label23;
    IBOutlet UILabel *label24;
    IBOutlet UILabel *label25;
    IBOutlet UILabel *label26;
    IBOutlet UILabel *label27;
    IBOutlet UILabel *label28;
    IBOutlet UILabel *label29;
    IBOutlet UILabel *label30;
    IBOutlet UILabel *label31;
    IBOutlet UILabel *label32;
    IBOutlet UILabel *label33;
    IBOutlet UILabel *label34;
    IBOutlet UILabel *label35;
    IBOutlet UILabel *label36;
    IBOutlet UILabel *label37;
    IBOutlet UILabel *label38;
    IBOutlet UILabel *label39;
    IBOutlet UILabel *label40;
    IBOutlet UILabel *label41;
    IBOutlet UILabel *label42;
    IBOutlet UILabel *label43;
    IBOutlet UILabel *label44;
    IBOutlet UILabel *label45;
    IBOutlet UILabel *label46;
    IBOutlet UILabel *label47;
    IBOutlet UILabel *label48;
    IBOutlet UILabel *label49;
    IBOutlet UILabel *label50;
    IBOutlet UILabel *label51;
    
    IBOutlet UILabel *labelAP;
    IBOutlet UILabel *labelVertical;
    IBOutlet UILabel *labelLateral;
    
    IBOutlet UILabel *los1;
    IBOutlet UILabel *los2;
    IBOutlet UILabel *los3;
    IBOutlet UILabel *los4;
    IBOutlet UILabel *los5;
    IBOutlet UILabel *los6;
    IBOutlet UILabel *los7;
    IBOutlet UILabel *los8;
    IBOutlet UILabel *los1in;
    IBOutlet UILabel *los2in;
    IBOutlet UILabel *los3in;
    IBOutlet UILabel *los4in;
    IBOutlet UILabel *los5in;
    IBOutlet UILabel *los6in;
    IBOutlet UILabel *los7in;
    IBOutlet UILabel *los8in;
    
    //    IBOutlet UILabel *puttTlabel;
    //    IBOutlet UILabel *puttSlabel;
    //    IBOutlet UILabel *arrow2label;
    //    IBOutlet UILabel *arrow4label;
    
    IBOutlet UIProgressView *vertical;
    IBOutlet UIProgressView *horizontal;
    IBOutlet UIProgressView *lvertical;
    IBOutlet UIProgressView *lhorizontal;
    IBOutlet UIProgressView *accvertical;
    IBOutlet UIProgressView *acchorizontal;
    IBOutlet UIProgressView *acc45deg;
    IBOutlet UISlider *verticals;
    IBOutlet UISlider *horizontals;
    IBOutlet UISlider *lhorizontals;
    IBOutlet UISlider *lverticals;
    IBOutlet UISlider *verticals2;
    IBOutlet UISlider *horizontals2;
    IBOutlet UISlider *lhorizontals2;
    IBOutlet UISlider *lverticals2;
    IBOutlet UISlider *acchorizontals;
    IBOutlet UISlider *accverticals;
    IBOutlet UISlider *acc45degs;
    IBOutlet UISlider *RLindicator;
    IBOutlet UISlider *RLs;
    IBOutlet UISlider *RLs2;
    IBOutlet UISlider *Lhind;
    IBOutlet UISlider *Rhind;
    IBOutlet UISlider *Lvind;
    IBOutlet UISlider *Rvind;
    
    IBOutlet UIButton *StartDrawButton;
    IBOutlet UIButton *StopDrawButton;
    IBOutlet UIButton *ClearDrawButton;
    IBOutlet UIButton *ChangeColor1Button;
    IBOutlet UIButton *ChangeColor2Button;
    IBOutlet UIButton *ChangeColor3Button;
    IBOutlet UIButton *ChangeColor4Button;
    IBOutlet UIButton *TSUIScanForPeripheralsButton;
    IBOutlet UIButton *TSUIScanForPeripheralsButtonL;
    IBOutlet UIButton *TSUIScanForPeripheralsButtonAcc;
    IBOutlet UIButton *DisplayIndicatorButton;
    IBOutlet UIButton *LimitsButton;
    IBOutlet UIButton *TargetSelectedButton;
    IBOutlet UIButton *AlarmButton;
    IBOutlet UIButton *GridButton;
    IBOutlet UIButton *ShoesButton;
    IBOutlet UIButton *ZeroFSRButton;
    IBOutlet UIButton *RawButton;
    IBOutlet UIButton *AccButton;
    IBOutlet UIButton *More1Button;
    IBOutlet UIButton *More2Button;
    IBOutlet UIButton *saveInfo;
    IBOutlet UIButton *retrieveInfo;
    IBOutlet UIButton *dispModeButton;
    IBOutlet UIButton *plotButton;
    IBOutlet UIButton *fileButton;
    IBOutlet UIButton *closePlotButton;
    IBOutlet UIButton *dev1PlotButton;
    IBOutlet UIButton *dev2PlotButton;
    IBOutlet UIButton *dev3PlotButton;
//    IBOutlet UIButton *FVcancelButton;
//    IBOutlet UIButton *FVdeleteButton;
//    IBOutlet UIButton *FVopenButton;
    IBOutlet UIButton *FVsaveButton;
//    IBOutlet UIButton *fileButton;
//    IBOutlet UIButton *PlotRecallButton;
//    IBOutlet UIButton *PlotSaveButton;
    IBOutlet UIButton *FSsaveButton;
//    IBOutlet UIButton *FScancelButton;
    
    IBOutlet UIButton *balanceButton;
    IBOutlet UIButton *feetButton;
    
    IBOutlet UIImageView *target;
    IBOutlet UIImageView *Ltarget;
    IBOutlet UIImageView *Rtarget;
    IBOutlet UIImageView *grid;
    IBOutlet UIImageView *alarm;
    IBOutlet UIImageView *drawing;
    IBOutlet UIImageView *background;
    IBOutlet UIImageView *accelerometer;
    IBOutlet UIImageView *accelerometerBatt;
    IBOutlet UIImageView *puttT;
    IBOutlet UIImageView *puttS;
    IBOutlet UIImageView *arrow2;
    IBOutlet UIImageView *arrow4;
    IBOutlet UIImageView *golfShoes;
    IBOutlet UIImageView *golfShoeR;
    IBOutlet UIImageView *golfShoeL;
    IBOutlet UIImageView *leftFoot;
    IBOutlet UIImageView *rightFoot;
    IBOutlet UIImageView *basePlate;
    IBOutlet UIImageView *balanceTracking;
    
    IBOutlet UITextField *textField;
    
    IBOutlet UITableView *tableView;
    
    UILabel *frequencyLabel;
    UILabel *volumeLabel;
    UILabel *feetLabel;
    UILabel *inchesLabel;
    UIButton *playButton;
    UISlider *frequencySlider;
    UISlider *volumeSlider;
    UISlider *patHeightSlider;
    UISlider *patStanceSlider;
    AudioComponentInstance toneUnit;
    
    IBOutlet UISwitch *dev1PlotSwitch;
    IBOutlet UISwitch *dev2PlotSwitch;
    IBOutlet UISwitch *dev3PlotSwitch;
    
@public
    double frequency;
    double volume;
    double patHeight;
    double patStance;
    double feet;
    double inches;
    double sampleRate;
    double theta;
    int dispMode;
    int tmpMode;
}

@property (nonatomic, strong) Patient *patient;

- (IBAction)btnBackClicked:(id)sender;
// UI elements actions
- (IBAction)TSUIScanForPeripheralsButton:(id)sender;
- (IBAction)TSUIScanForPeripheralsButtonL:(id)sender;
- (IBAction)TSUIScanForPeripheralsButtonAcc:(id)sender;
- (IBAction)TSUISoundBuzzerButton:(id)sender;
- (IBAction)StartDrawButton;
- (IBAction)StopDrawButton;
- (IBAction)ClearDrawButton;
- (IBAction)ChangeColor1Button;
- (IBAction)ChangeColor2Button;
- (IBAction)ChangeColor3Button;
- (IBAction)ChangeColor4Button;
- (IBAction)DisplayIndicatorButton;
- (IBAction)LimitsButton;
- (IBAction)TargetSelectedButton;
- (IBAction)AlarmButton;
- (IBAction)GridButton;
- (IBAction)ShoesButton;
- (IBAction)ZeroFSRButton;
- (IBAction)RawButton;
- (IBAction)AccButton;
- (IBAction)More1Button;
- (IBAction)More2Button;

- (IBAction)GraphOnlyButton;

- (IBAction)saveInfo:(id)sender;
- (IBAction)retrieveInfo:(id)sender;
- (IBAction)eMailInfo:(id)sender;

- (IBAction)dispModeButton;
- (IBAction)plotButton;
- (IBAction)closePlotButton;

- (IBAction)FVcancelButton;
- (IBAction)FVdeleteButton;
- (IBAction)FVopenButton;
- (IBAction)FVsaveButton;
- (IBAction)fileButton;
- (IBAction)PlotRecallButton;
- (IBAction)PlotSaveButton;
- (IBAction)FSsaveButton;
- (IBAction)FScancelButton;

- (IBAction)balanceButton;
- (IBAction)feetButton;


//- (IBAction)dev1PlotButton;
//- (IBAction)dev2PlotButton;
//- (IBAction)dev3PlotButton;

//- (IBAction)switchPlotSwitch:(id)sender;
- (IBAction)switchPlotSwitch;

// UI elements outlets
@property (weak, nonatomic) IBOutlet UIProgressView *TSUIBatteryBar;
@property (weak, nonatomic) IBOutlet UILabel *TSUIBatteryBarLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *TSUIBatteryBarL;
@property (weak, nonatomic) IBOutlet UIProgressView *TSUIBatteryBarAcc;
//@property (weak, nonatomic) IBOutlet UILabel *TSUIBatteryBarLabelL;
@property (weak, nonatomic) IBOutlet UISwitch *TSUILeftButton;
@property (weak, nonatomic) IBOutlet UISwitch *TSUIRightButton;
@property (weak, nonatomic) IBOutlet UISwitch *TSUIAccButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *TSUISpinner;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *TSUISpinnerL;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *TSUISpinnerAcc;
//@property(nonatomic, readonly) CIColor *CIColor;

@property (nonatomic,retain) UIImageView *target;
@property (nonatomic,retain) UIImageView *Ltarget;
@property (nonatomic,retain) UIImageView *Rtarget;
@property (nonatomic,retain) UIImageView *grid;
@property (nonatomic,retain) UIImageView *alarm;
@property (nonatomic,retain) UIImageView *accelerometer;
@property (nonatomic,retain) UIImageView *background;
@property (nonatomic,retain) UIImageView *accelerometerBatt;
@property (nonatomic,retain) UIImageView *puttT;
@property (nonatomic,retain) UIImageView *puttS;
@property (nonatomic,retain) UIImageView *arrow2;
@property (nonatomic,retain) UIImageView *arrow4;
@property (nonatomic,retain) UIImageView *golfShoes;
@property (nonatomic,retain) UIImageView *golfShoeR;
@property (nonatomic,retain) UIImageView *golfShoeL;
@property (nonatomic,retain) UIImageView *rightFoot;
@property (nonatomic,retain) UIImageView *leftFoot;
@property (nonatomic,retain) UIImageView *basePlate;

@property (nonatomic,retain) IBOutlet UISlider *RLindicator;
@property (nonatomic,retain) IBOutlet UISlider *Lhind;
@property (nonatomic,retain) IBOutlet UISlider *Rhind;
@property (nonatomic,retain) IBOutlet UISlider *Lvind;
@property (nonatomic,retain) IBOutlet UISlider *Rvind;

@property (nonatomic, retain) IBOutlet UISlider *horizontals;
- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *verticals;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *lhorizontals;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *lverticals;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *horizontals2;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *verticals2;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *lhorizontals2;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *lverticals2;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *acchorizontals;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *accverticals;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *acc45degs;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *RLs;
//- (IBAction)sliderValueChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UISlider *RLs2;
//- (IBAction)sliderValueChanged:(id)sender;

//@property (retain, nonatomic) IBOutlet UIButton *StartDrawButton;
//@property (retain, nonatomic) IBOutlet UIButton *StopDrawButton;
//@property (retain, nonatomic) IBOutlet UIButton *ClearDrawButton;

@property (strong, nonatomic) IBOutlet UITextView *resultsView;
@property (strong, nonatomic) IBOutlet UIView *setupView;
@property (strong, nonatomic) IBOutlet UIView *balanceView;
@property (strong, nonatomic) IBOutlet UIView *plotView;
@property (strong, nonatomic) IBOutlet UIView *fileView;
@property (strong, nonatomic) IBOutlet UIView *file2View;
@property (strong, nonatomic) IBOutlet UIView *footView;
@property (strong, nonatomic) IBOutlet UIView *fileSaveView;

//Timer methods
- (void) batteryIndicatorTimer:(NSTimer *)timer;
- (void) batteryIndicatorTimerL:(NSTimer *)timer;
- (void) batteryIndicatorTimerAcc:(NSTimer *)timer;
- (void) connectionTimer:(NSTimer *)timer;
- (void) connectionTimerL:(NSTimer *)timer;
- (void) connectionTimerAcc:(NSTimer *)timer;

@property (nonatomic, retain) IBOutlet UISlider *frequencySlider;
@property (nonatomic, retain) IBOutlet UISlider *volumeSlider;
@property (nonatomic, retain) IBOutlet UISlider *patHeightSlider;
@property (nonatomic, retain) IBOutlet UISlider *patStanceSlider;
@property (nonatomic, retain) IBOutlet UIButton *playButton;
@property (nonatomic, retain) IBOutlet UILabel *frequencyLabel;
@property (nonatomic, retain) IBOutlet UILabel *volumeLabel;
@property (nonatomic, retain) IBOutlet UILabel *patHeightLabel;
@property (nonatomic, retain) IBOutlet UILabel *patStanceLabel;
@property (nonatomic, retain) IBOutlet UILabel *feetLabel;
@property (nonatomic, retain) IBOutlet UILabel *inchesLabel;

//- (IBAction)sliderChanged:(UISlider *)frequencySlider;
//- (IBAction)sliderChanged:(UISlider *)volumeSlider;
- (IBAction)togglePlay:(UIButton *)selectedButton;
- (void)stop;

//@property (strong, nonatomic) IBOutlet UILabel *textFieldText;
@property (nonatomic, retain) IBOutlet UITextField *textFieldText;

//@property (nonatomic, retain) IBOutlet UITableViewCell *tableViewCell;

@end
