//
//  DetailViewController.m
//  CoredataDemo
//
//  Created by Asad Ali on 04/12/2014.
//  Copyright (c) 2014 DevBatch. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UISwitch *statusSwitch;



@end

@implementation DetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Patient Details";

    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Buttons Action

- (IBAction)btnSaveClicked:(id)sender
{
    if (!self.patient)
    {
        self.patient = (Patient *)[Patient create];
    }
    
    self.patient.patientId = self.txtDescription.text;
    self.patient.patientName = self.txtTitle.text;
    self.patient.patientDoB = self.datePicker.date;
    self.patient.patientIsActive = @(self.statusSwitch.isOn);
    
    [Patient save];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Managing the detail item

- (void)configureView
{
    // Update the user interface for the detail item.
    if (self.patient)
    {
        self.txtDescription.text = self.patient.patientId;
        self.txtTitle.text = self.patient.patientName;
        self.datePicker.date = self.patient.patientDoB;
        [self.statusSwitch setOn:[self.patient.patientIsActive boolValue]];
    }
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtTitle)
    {
        [self.txtDescription becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}


@end
