//
//  main.m
//  TruSensors
//

#import <UIKit/UIKit.h>

#import "TSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TSAppDelegate class]));
    }
}
