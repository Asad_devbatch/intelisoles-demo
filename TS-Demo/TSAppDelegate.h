//
//  TSAppDelegate.h
//  TruSensors
//

#import <UIKit/UIKit.h>
#import "TruSensors.h"

@interface TSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TruSensors *leftInsole;
@property (strong, nonatomic) TruSensors *rightInsole;


@end
